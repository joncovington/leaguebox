leaguebox

An organization management application for amateur league servicing

Features for pre-alpha

- [x]Custom User Model
- [ ]Further All-Auth implementation with social auth
- [x]Organization Application
- [x]Invitations to Roles
- [x]Post/Social system for Organizations
- [x]Services/Products implementation
    - [x]Service/Product Detail Page
- [x]Event Scheduling/Applicaiton/Review
    - [x] Create Post after Event Approval
- [x]Event Invoicing
- [x]Payment Processing
- [x]Event Staff Initialization
    - [x] Notify Organization Staff Roles of Event Staff Registration Open
- [x]Event Staff Registration
- [x]Event/Game Initialization
- [x]Event/Game User Registration
- [x]Event/Game Process
- [x]Post Event/Game Placement Processing
- [x]Sub Event/Game Creation by Organization Role Users
- [x]Event/Game Closure
- [x]VIP Registration
- [ ]Other VIP Features
- [ ]Inventory Tracking
- [ ]Legal/Policies/Use Agreements

- [ ]Test development

other features to consider
- [ ]Django REST refactor for use with ReactJS
- [ ]django-unicorn library?