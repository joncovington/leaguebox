from django.apps import AppConfig


class AppEquipmentConfig(AppConfig):
    name = 'App_Equipment'
