from django.contrib import admin
from App_Equipment.models import Equipment
# Register your models here.


@admin.register(Equipment)
class EquipmentAdmin(admin.ModelAdmin):
    list_display = ('serial', 'organization', 'view_product_name', 'location')
    list_display_links = ('serial', 'view_product_name')
    list_editable = ('location', )

    def view_product_name(self, obj):
        return obj.product.name
