from django.db import models
from App_Organization.models import Organization
from App_Shop.models import Product

# Create your models here.


class Equipment(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.DO_NOTHING, related_name='equip_orgs')
    serial = models.CharField(max_length=255)
    product = models.ForeignKey(Product, on_delete=models.DO_NOTHING)
    location = models.ForeignKey(Organization, on_delete=models.DO_NOTHING, related_name='location_orgs')
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = [['organization', 'serial']]
        verbose_name_plural = 'Equipment'

    def __str__(self):
        return f'{self.organization} {self.product.name}: {self.serial}'
