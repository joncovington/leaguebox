import re
from math import ceil
from .regex_generator import generator
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.db import models
from App_Event.models import SubEvent, SubEventRegistration
from App_Equipment.models import Equipment
from App_Organization.models import Organization

# Create your models here.


class GameType(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Game(models.Model):
    subevent = models.OneToOneField(SubEvent, on_delete=models.CASCADE, primary_key=True)
    game_type = models.ForeignKey(GameType, on_delete=models.DO_NOTHING)
    created = models.DateTimeField(auto_now_add=True)
    start = models.DateTimeField(null=True, blank=True)
    end = models.DateTimeField(null=True, blank=True)
    auto_assign_seats = models.BooleanField(default=False)

    @property
    def ranked_users(self):
        return GameRank.objects.filter(game=self).count()

    @property
    def base_points(self):
        game_rank_users = GameRank.objects.filter(game=self)
        if game_rank_users:
            game_rank_count = game_rank_users.count()
            # calculate base points
            base = game_rank_count / 10
            base = ceil(base)
            base = base * 10
            return base
        else:
            return 0

    def __str__(self):
        return f'Event:{self.subevent.event.id} {self.subevent.type} {self.game_type}'


class GameRank(models.Model):
    game = models.ForeignKey(Game, on_delete=models.DO_NOTHING, related_name='game_rank')
    registration = models.ForeignKey(SubEventRegistration, on_delete=models.DO_NOTHING)
    rank = models.PositiveSmallIntegerField(null=True, blank=True, validators=[MinValueValidator(1), ])
    points = models.PositiveSmallIntegerField(null=True, blank=True, validators=[MinValueValidator(1), ])
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Game Rankings'
        unique_together = ('game', 'registration')
        ordering = ('rank', 'created')

    def __str__(self):
        return f'Event:{self.game.subevent.event.id} {self.game.subevent.type} {self.game.game_type} {self.registration.user.profile.display_name}'


class GameEquip(models.Model):
    '''
    Table for managing active equipment and moving
    players/registered game users within a game through GameSeats
    '''
    equip = models.ForeignKey(Equipment, on_delete=models.DO_NOTHING)
    game = models.ForeignKey(Game, on_delete=models.DO_NOTHING, related_name='game_equip')
    label = models.SmallIntegerField()

    class Meta:
        verbose_name_plural = 'Game Equipment'
        unique_together = ('equip', 'game')

    def __str__(self):
        return f'{self.game.subevent.event.start.strftime("%d%b%Y")} {self.equip}'

    @property
    def total_seats(self):
        return self.game_seat.all().count()
        # return GameSeat.objects.filter(game_equip=self).count()

    @property
    def empty_seats(self):
        return self.game_seat.filter(seat_user__isnull=True).count()

    @property
    def filled_seats(self):
        return self.game_seat.filter(seat_user__isnull=False).count()

    @property
    def is_empty(self):
        return self.total_seats == self.empty_seats


class GameSeat(models.Model):
    game_equip = models.ForeignKey(GameEquip, on_delete=models.DO_NOTHING, related_name='game_seat')
    seat = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), ])
    seat_user = models.ForeignKey(SubEventRegistration, on_delete=models.DO_NOTHING, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Game Seats'
        unique_together = ('game_equip', 'seat')

    def __str__(self):
        return f'{self.game_equip.equip.product.name}: #{self.game_equip.label} Seat: #{self.seat} {self.game_equip.game.subevent.type} {self.game_equip.game.game_type}: {self.seat_user}'


class RankSet(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.organization.organization_name} | {self.name}'


class RankRange(models.Model):
    rank_set = models.ForeignKey(RankSet, on_delete=models.CASCADE)
    start = models.PositiveSmallIntegerField(help_text='Minimum number of entries', validators=[MinValueValidator(1), ])
    end = models.PositiveSmallIntegerField(help_text='Maximum number of entries')
    regex = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        ordering = ('rank_set', 'start',)

    def clean(self) -> None:

        if self.start > self.end:
            raise ValidationError(_('Range start must be less than range end.'))

        rank_calc_qs = RankRange.objects.filter(rank_set=self.rank_set)
        if self.regex is not None:
            print(self.regex)
            print(generator().numerical_range(self.start, self.end))

        # if range has not changed, skip check for matching regex to another RankRange
        if not self.regex == generator().numerical_range(self.start, self.end):
            for rank_calc_item in rank_calc_qs:
                if re.match(rank_calc_item.regex, str(self.start)) or re.match(rank_calc_item.regex, str(self.end)):
                    raise ValidationError(_('Range must not overlap other ranges for rank calculation.'))
        return super(RankRange, self).clean()

    def save(self, *args, **kwargs):
        generate = generator()
        regex = generate.numerical_range(self.start, self.end)
        self.regex = regex
        super(RankRange, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.rank_set.name} | {self.start} to {self.end}'


class RankStructure(models.Model):
    rank_range = models.ForeignKey(RankRange, on_delete=models.CASCADE)
    place = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), ], blank=True, null=True)
    percentage = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(100)], blank=True, null=True)
    flat_points = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), ], blank=True, null=True)

    class Meta:
        ordering = ('rank_range', 'place',)
        unique_together = ('rank_range', 'place')

    def clean(self):
        if self.percentage and self.flat_points:
            raise ValidationError('Please use either Percentage or Flat Points. Cannot save both values in structure.')
        if not self.percentage and not self.flat_points:
            raise ValidationError('Structure must have a value either Percentage or Flat Points.')
        return super().clean()

    def __str__(self):
        place_text = 'All Others'
        if self.place:
            place_text = 'Place #' + str(self.place)
        return f'{self.rank_range} | {place_text} | {self.percentage}%'
