from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls.base import reverse
from django.views.generic.base import TemplateView, View
from django.contrib import messages
from django.utils import timezone
from App_Equipment.models import Equipment
from App_Game.models import GameEquip, GameRank, GameSeat, GameType, Game
from App_Event.models import SubEvent, SubEventRegistration

from App_Event.views import require_seat_assignment, IsEventStaffMixin


# Create your views here.


class CreateGame(View):
    '''Accept ajax request to create a game and populate GameEquip with seats'''
    def post(self, request):
        if request.is_ajax():
            json_data = request.POST
            game_type = GameType.objects.get(id=json_data['gameTypeOption'])
            subevent = get_object_or_404(SubEvent, id=json_data['subevent'])

            auto_assign_seats = False
            if json_data['autoAssignSeats'] == 'true':
                auto_assign_seats = True

            # create a new game
            new_game = Game.objects.create(
                subevent=subevent,
                game_type=game_type,
                auto_assign_seats=auto_assign_seats,
            )
            new_game.save()

            # set up each piece of equipment and add default_seats
            equip = Equipment.objects.filter(id__in=json_data.getlist('equipmentSelected[]'))
            seats_created = 0
            equip_label = 0
            for item in equip:
                thing = get_object_or_404(Equipment, id=item.id)
                new_game_equip = None
                new_game_equip = GameEquip.objects.create(
                    equip=thing,
                    game=new_game,
                    label=(equip_label+1)
                )
                new_game_equip.save()
                equip_label += 1
                for x in range(thing.product.default_seats):
                    new_game_seat = None
                    new_game_seat = GameSeat.objects.create(
                        game_equip=new_game_equip,
                        seat=(x+1),
                    )
                    new_game_seat.save()
                    seats_created += 1

            # open subevent for registration
            allow_vip = False
            if json_data['allowVip'] == 'true':
                allow_vip = True
            subevent.allow_coin_boost = allow_vip
            subevent.status = 'open'
            subevent.save()
        return JsonResponse({'status': 200, 'gameId': new_game.pk})
        # return JsonResponse({'status': 200})


class GameSeatAssign(LoginRequiredMixin, TemplateView):
    template_name = 'App_Game/choose_seat.html'

    def dispatch(self, request, *args, **kwargs):
        self.game = get_object_or_404(Game, pk=kwargs['pk'])
        subevent = SubEvent.objects.get(id=self.game.subevent.id)
        url = HttpResponseRedirect(reverse('App_Event:sub_review', kwargs={'pk': subevent.pk}))

        # If user has already been assigned a seat, redirect to subevent review
        open_seat, has_seat = require_seat_assignment(subevent, request.user)
        if not open_seat:
            messages.add_message(request, messages.ERROR, 'There are no seats currently available. Seats will be assigned as availability changes.')
            return url
        if has_seat:
            messages.add_message(request, messages.WARNING, 'You already have a seat assignment.')
            return url
        game_equip = GameEquip.objects.filter(game=self.game)

        # query for equipment with available seats
        # todo: replace with get_available_game_equipment()
        self.seats_qs = GameSeat.objects.filter(game_equip__in=game_equip, seat_user__isnull=True)
        self.available_seats_qs = self.seats_qs.values_list('game_equip', flat=True).distinct()
        if not self.available_seats_qs:
            messages.add_message(request, messages.ERROR, 'There are no available seats for this game. Please see event staff.')
            # return url
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = {}
        context['page_title'] = 'Choose A Seat'
        context['game'] = self.game

        available_game_equip = GameEquip.objects.filter(id__in=self.available_seats_qs).order_by('label')
        context['game_equip'] = available_game_equip
        return context


class GetOpenSeats(View):
    '''Return open seats for a specific GameEquip'''
    def post(self, request):
        if request.is_ajax():
            json_data = request.POST
            if json_data['equipId']:
                game_equip = GameEquip.objects.get(id=json_data['equipId'])
                available_seats = GameSeat.objects.filter(game_equip=game_equip, seat_user__isnull=True).values_list('seat', flat=True)
                available_seats = list(available_seats)
                print(available_seats)
                return JsonResponse({'status': 200, 'seat_list': sorted(available_seats)})
            else:
                available_seats = []
                return JsonResponse({'status': 200, 'seat_list': available_seats})
        return JsonResponse({'status': 'false'}, status=500)


class ReserveSeat(View):
    '''Assigns requested seat to user'''
    def post(self, request):
        json_data = request.POST
        print(json_data)

        game = Game.objects.get(pk=json_data['gameId'])

        # check that user is registered for subevent
        if json_data['regId']:
            registration = SubEventRegistration.objects.filter(pk=json_data['regId'])
        else:
            registration = SubEventRegistration.objects.filter(subevent=game.subevent, user=request.user)
        if registration:
            registration = registration[0]
            game_equip = GameEquip.objects.filter(id=json_data['equipId'])
            game_seat = GameSeat.objects.filter(game_equip=game_equip[0], seat=json_data['seatId'])
            print(game_seat)
            if game_seat:
                game_seat = game_seat[0]
                # check that seat is actually available.
                if game_seat.seat_user is None:
                    game_seat.seat_user = registration
                    game_seat.save()
                    response_data = {}
                    response_data['status'] = 200
                    response_data['result'] = 'Seat Assigned Successfully'
                    return JsonResponse(response_data)
        return JsonResponse({'status': 'false'}, status=500)


def assign_seat(game_equip_id, seat, registration):
    if registration.closed:
        print('This registration is closed and cannot be assigned to a seat')
        return False
    game_equip = GameEquip.objects.filter(id=game_equip_id)
    game_seat = GameSeat.objects.filter(game_equip=game_equip[0], seat=seat)
    if game_seat:
        game_seat = game_seat[0]
        # check that seat is actually available.
        if game_seat.seat_user is None:
            game_seat.seat_user = registration
            game_seat.save()
            print(game_seat)
            return True


def clear_seat(game_seat, user):
    if game_seat.seat_user.user == user:
        game_seat.seat_user = None
        game_seat.save()
        return True


class MoveSeat(View):
    '''Moves user from one seat to another'''
    def post(self, request):
        json_data = request.POST
        print(json_data)

        # make sure new seat is not already reserved
        new_seat_check = GameSeat.objects.get(game_equip=json_data['equipId'], seat=json_data['seatId'])
        print(new_seat_check)
        if new_seat_check.seat_user is not None:
            return JsonResponse({'status': 'false'}, status=500)
        # get the old seat info
        game_seat = GameSeat.objects.get(id=json_data['userSeatId'])
        registration = game_seat.seat_user
        # remove the registration from the game seat
        game_seat.seat_user = None
        game_seat.save()

        if assign_seat(json_data['equipId'], json_data['seatId'], registration):
            return JsonResponse({'status': '200'}, status=200)
        return JsonResponse({'status': 'false'}, status=500)


class ClearSeat(View):
    def post(self, request):
        json_data = request.POST
        print(json_data)
        if json_data['seatId']:
            # clear seat user
            # check request comes from the current seat user
            game_seat = GameSeat.objects.get(pk=json_data['seatId'])
            print(game_seat)
            if game_seat.seat_user.user == request.user:
                if clear_seat(game_seat, request.user):
                    response_data = {}
                    response_data['status'] = 200
                    response_data['result'] = 'Seat Unassigned Successfully'
                    return JsonResponse(response_data, status=200)
            return JsonResponse({'status': 'false'}, status=500)
        else:
            return JsonResponse({'status': 'false'}, status=500)


def get_available_game_equip(game):
    game_equip = GameEquip.objects.filter(game=game).order_by('label')
    seats_qs = GameSeat.objects.filter(game_equip__in=game_equip, seat_user__isnull=True)
    available_seats_qs = seats_qs.values_list('game_equip', flat=True).distinct()
    available_game_equip = GameEquip.objects.filter(id__in=available_seats_qs).order_by('label')
    return available_game_equip


class GameSectionView(LoginRequiredMixin, TemplateView):
    '''Simple view to display users assigned to game seats within a game equip'''
    template_name = 'App_Game/game_section.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        game_section = get_object_or_404(GameEquip, pk=kwargs['pk'])
        context['game_section'] = game_section
        available_equip = get_available_game_equip(game_section.game)
        context['game_equip'] = available_equip
        return context


class ResolveGameSeat(IsEventStaffMixin, LoginRequiredMixin, View):
    '''
    Event staff finalizes a users place in a game. Game seat cleared,
     record added to game rank model and subevent registration closed.
    '''
    def get(self, request, pk):
        # pk for this get is the GameSeat
        game_seat = get_object_or_404(GameSeat, pk=pk)
        record_datetime = timezone.now()

        # get the registration record and close/date
        registration = game_seat.seat_user
        registration.closed = True
        registration.closed_date = record_datetime
        registration.save()

        # create a game rank record
        game_rank = GameRank.objects.create(
            game=game_seat.game_equip.game,
            registration=registration,
        )
        game_rank.save()

        # remove the registration from the game seat
        game_seat.seat_user = None
        game_seat.save()

        return HttpResponseRedirect(reverse('App_Game:game_section', kwargs={'pk': game_seat.game_equip.pk}))


class ResolveGameEquip(IsEventStaffMixin, LoginRequiredMixin, View):
    '''
    Event staff closes a game equip after no longer needed in a subevent.
    '''
    def get(self, request, pk):
        # pk for this get is the GameEquip
        game_equip = get_object_or_404(GameEquip, pk=pk)
        subevent = game_equip.game.subevent
        seat_list = []
        # verify equip has no more assigned seats before removing seats
        if game_equip.is_empty:
            for seat in game_equip.game_seat.all():
                if seat.seat_user is None:
                    seat_list.append(seat.seat)
                    seat.delete()
            # build message
            seat_list.sort()
            seat_msg, seat_msg2 = ('Seat', 'has')
            if len(seat_list) > 1:
                seat_msg, seat_msg2 = ('Seats', 'have')
            seat_str = ''
            seat_str = ', '.join(map(str, seat_list))
            print(seat_str)
            messages.add_message(request, messages.SUCCESS, f'{seat_msg} {seat_str} {seat_msg2} been removed.')
            messages.add_message(request, messages.SUCCESS, f'{game_equip.equip.product.name} #{game_equip.label} has been removed.')
            game_equip.delete()
        else:
            messages.add_message(request, messages.ERROR, f'{game_equip.equip.product.name} #{game_equip.label} could not be removed.')
        return HttpResponseRedirect(reverse('App_Event:sub_review_tab', kwargs={'pk': subevent.pk, 'tab': 'sections'}))


class StartGame(IsEventStaffMixin, LoginRequiredMixin, View):

    def get(self, request, pk):
        game = get_object_or_404(Game, pk=pk)
        if not game.subevent.finalized:
            if not game.start:
                game.start = timezone.now()
                game.save()
                messages.add_message(request, messages.SUCCESS, 'Success! Game Started!')
            else:
                messages.add_message(request, messages.ERROR, 'Error starting game. Game has already started.')

        return HttpResponseRedirect(reverse('App_Event:sub_review_tab', kwargs={'pk': game.subevent.pk, 'tab': 'actions'}))
