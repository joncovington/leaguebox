# Generated by Django 3.1.5 on 2021-04-01 16:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('App_Game', '0018_auto_20210329_1657'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='gamerank',
            options={'ordering': ('rank', 'created'), 'verbose_name_plural': 'Game Rankings'},
        ),
    ]
