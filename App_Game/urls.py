from django.urls import path
from App_Game import views

app_name = 'App_Game'

urlpatterns = [
    path('create/', views.CreateGame.as_view(), name='create'),
    path('<pk>/start/', views.StartGame.as_view(), name='start'),
    path('<pk>/choose-your-seat/', views.GameSeatAssign.as_view(), name='choose_seat'),
    path('get-open-seats/', views.GetOpenSeats.as_view(), name='get_seats'),
    path('reserve-seat/', views.ReserveSeat.as_view(), name='reserve_seat'),
    path('unassign-seat/', views.ClearSeat.as_view(), name='unassign_seat'),
    path('move-seat/', views.MoveSeat.as_view(), name='move_seat'),
    path('game-section/<pk>/', views.GameSectionView.as_view(), name='game_section'),
    path('seat/<pk>/resolve/', views.ResolveGameSeat.as_view(), name='resolve_seat'),
    path('section/<pk>/resolve/', views.ResolveGameEquip.as_view(), name='resolve_equip'),
]
