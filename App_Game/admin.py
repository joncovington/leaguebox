from django.contrib import admin

from App_Game.models import Game, GameEquip, GameRank, GameSeat, GameType, RankRange, RankSet, RankStructure


@admin.register(GameSeat)
class GameSeatAdmin(admin.ModelAdmin):
    list_display = ('event_date', 'event', 'subevent', 'provider', 'host', 'equipment', 'seat', 'seat_user')
    ordering = ('game_equip', 'seat')

    def equipment(self, obj):
        return f'{obj.game_equip.equip.product.name} #{obj.game_equip.label}'

    def event(self, obj):
        return obj.game_equip.game.subevent.event.id

    def event_date(self, obj):
        return obj.game_equip.game.subevent.event.start.strftime('%Y-%m-%d')

    def provider(self, obj):
        return obj.game_equip.game.subevent.event.organization

    def host(self, obj):
        if obj.game_equip.game.subevent.event.user_org:
            return obj.game_equip.game.subevent.event.user_org
        else:
            return None

    def subevent(self, obj):
        return obj.game_equip.game.subevent.type


@admin.register(RankRange)
class RankRangeAdmin(admin.ModelAdmin):
    readonly_fields = ('regex', )


@admin.register(GameRank)
class GameRankAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'points')
    list_display = ('game', 'rank', 'user', 'points')

    def user(self, obj):
        return f'{obj.registration.user.profile.get_profile_firstlast()} | {obj.registration.user.profile.display_name}'


admin.site.register(GameType)
admin.site.register(Game)
admin.site.register(GameEquip)
admin.site.register(RankSet)
admin.site.register(RankStructure)
