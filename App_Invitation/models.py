import uuid
from django.db import models
from App_Organization.models import Role, Organization
from App_Login.models import User
# Create your models here.


class Invitation(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='invite_sender')
    email = models.EmailField()
    sent_date = models.DateTimeField(null=True, blank=True)
    expire_date = models.DateTimeField(null=True, blank=True)
    accepted = models.BooleanField(default=False)
    approved_date = models.DateTimeField(null=True, blank=True)
    archived = models.BooleanField(default=False)
    rejected = models.BooleanField(default=False)
    rejected_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return f'{self.email}'


class OrganizationInvitation(models.Model):
    invitation = models.ForeignKey(Invitation, on_delete=models.CASCADE, related_name='invitation')
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, related_name='invitation_organization')
    roles = models.ManyToManyField(Role)

    def __str__(self):
        return f'{self.invitation.email} | {self.organization.organization_name}'
