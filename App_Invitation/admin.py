from django.contrib import admin
from App_Invitation.models import Invitation, OrganizationInvitation
from App_Invitation.forms import OrganizationInvitationAdminForm
# Register your models here.


class OrganizationInvitationAdmin(admin.ModelAdmin):
    form = OrganizationInvitationAdminForm
    list_display = ('organization', 'invitation',)


admin.site.register(Invitation)
admin.site.register(OrganizationInvitation, OrganizationInvitationAdmin)
