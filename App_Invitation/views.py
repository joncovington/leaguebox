import datetime

from django.shortcuts import render, HttpResponseRedirect, reverse
from django.contrib.auth.decorators import login_required
from App_Organization.models import Organization
from django.conf import settings
from django.contrib import messages
from django.utils import timezone

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template

from django.contrib.sites.shortcuts import get_current_site

from App_Organization.models import Staff

from App_Invitation.models import OrganizationInvitation, Invitation
from App_Invitation.forms import OrganizationInvitationForm, InvitationForm

from App_Login.models import User

# Create your views here.


@login_required
def start_invitation(request, slug):
    context = {}
    org_type = Organization.objects.get(slug=slug).orgtype.orgtype
    context['page_title'] = 'New ' + org_type + ' Invitation'
    invite_form = InvitationForm()
    org_invite_form = OrganizationInvitationForm(org_type=org_type)
    context['form'] = invite_form
    context['form2'] = org_invite_form
    if request.method == 'POST':
        invite_form = InvitationForm(request.POST)
        org_invite_form = OrganizationInvitationForm(request.POST, org_type=org_type)
        context['form'] = invite_form
        context['form2'] = org_invite_form
        if invite_form.is_valid() and org_invite_form.is_valid():

            # Check if an invite already exists
            email = invite_form.cleaned_data['email']
            organization = Organization.objects.get(slug=slug)
            check_invitations = OrganizationInvitation.objects.filter(invitation__email=email, organization=organization)
            for invite in check_invitations:
                for role in invite.roles.all():
                    for form_role in org_invite_form.cleaned_data['roles']:
                        if role.role == form_role.role:
                            messages.warning(request, f'The invitation for {email} already exists.')
                            return render(request, 'App_Invitation/invite.html', context=context)

            # Check for existing user with email and role
            existing_user = User.objects.get(email=email)
            existing_staff_list = Staff.objects.filter(user=existing_user, organization=organization)
            if len(existing_staff_list) > 0:
                existing_staff = Staff.objects.get(user=existing_user, organization=organization)
                exception_count = 0
                for existing_roles in existing_staff.roles.all():
                    for form_role in org_invite_form.cleaned_data['roles']:
                        if existing_roles.role == form_role.role:
                            exception_count += 1
                            messages.warning(request, f"The role '{form_role.role}' for {email} already exists.")
                if exception_count > 0:
                    return render(request, 'App_Invitation/invite.html', context=context)

            # create invitation
            invitation = Invitation()
            invitation.email = email
            invitation.sender = request.user
            invitation.sent_date = timezone.now()
            invitation.expire_date = timezone.now()
            invitation.expire_date = invitation.expire_date + datetime.timedelta(days=7)
            invitation.save()
            organization_invitation = OrganizationInvitation()
            organization_invitation.invitation = invitation
            organization_invitation.organization = organization
            organization_invitation.save()
            organization_invitation.roles.set(org_invite_form.cleaned_data['roles'])
            organization_invitation.save()

            # Send invitation to email
            current_site = get_current_site(request)
            plaintext_email = get_template('App_Invitation/email/new_invitation.txt')
            html_email = get_template('App_Invitation/email/new_invitation.html')
            email_context = {
                'organization': organization_invitation.organization.organization_name,
                'sender': request.user.profile.get_profile_firstlast(),
                'roles': organization_invitation.roles,
                'site': reverse('App_Organization:home'),
                'signup': reverse('App_Login:signup'),
                'domain': current_site
            }
            subject, from_email, to_email = 'You have a new invitation!', settings.NO_REPLY_EMAIL, invitation.email
            text_content = plaintext_email.render(email_context)
            html_content = html_email.render(email_context)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to_email])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            messages.success(request, f'Invitation sent to {invitation.email}.')
            return HttpResponseRedirect(reverse('App_Organization:invite', args=(), kwargs={'slug': slug}))

    return render(request, 'App_Invitation/invite.html', context=context)


@login_required
def user_invites(request):
    context = {}
    context['page_title'] = f"{request.user.profile.get_profile_firstlast()}'s Invitations"
    user_invites = OrganizationInvitation.objects.filter(invitation__email=request.user.email, invitation__expire_date__gte=timezone.now())
    if user_invites.exists():
        context['invites'] = user_invites
    else:
        messages.info(request, 'You have no pending invitations.')
    return render(request, 'App_Invitation/my_invites.html', context=context)


@login_required
def accept_invite(request, pk):
    invitation = Invitation.objects.get(pk=pk)
    invite_org = OrganizationInvitation.objects.get(invitation=invitation)
    email = invite_org.invitation.email
    invite_user = User.objects.get(email=email)
    org_staff_member, created = Staff.objects.get_or_create(
        user=invite_user,
        organization=invite_org.organization,
    )
    # Add organization invitation role to organization staff user roles
    for role in invite_org.roles.all():
        org_staff_member.roles.add(role.id)
    # Delete the invitation
    messages.success(request, f'You have accepted invitation {invitation.id}')
    Invitation.objects.get(pk=pk).delete()

    return HttpResponseRedirect(reverse('App_Organization:my_invites'))


@login_required
def decline_invite(request, pk):
    # Delete the invitation
    invitation = Invitation.objects.get(pk=pk)
    messages.warning(request, f'You have declined invitation {invitation.id}')
    invitation.delete()

    return HttpResponseRedirect(reverse('App_Organization:my_invites'))
