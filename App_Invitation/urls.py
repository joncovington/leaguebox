from django.urls import path
from App_Invitation import views

app_name = 'App_Invitation'

urlpatterns = [
    path('my-invites/', views.user_invites, name='my_invites'),
    path('<slug>/invite/', views.start_invitation, name='invite'),
    path('<pk>/accept/', views.accept_invite, name='accept_invite'),
    path('<pk>/decline/', views.decline_invite, name='decline_invite'),
]
