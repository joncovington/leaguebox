from django.apps import AppConfig


class AppInvitationConfig(AppConfig):
    name = 'App_Invitation'
