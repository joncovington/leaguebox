from django import forms
from App_Invitation.models import OrganizationInvitation, Invitation
from App_Organization.models import Role


class InvitationForm(forms.ModelForm):
    class Meta:
        model = Invitation
        fields = ('email',)


class OrganizationInvitationForm(forms.ModelForm):

    class Meta:
        model = OrganizationInvitation
        fields = ('roles',)

    def __init__(self, *args, **kwargs):
        org = kwargs.pop('org_type')
        super(OrganizationInvitationForm, self).__init__(*args, **kwargs)
        self.fields['roles'].queryset = Role.objects.filter(role_type__orgtype=org)
        self.fields['roles'].widget = forms.CheckboxSelectMultiple(attrs={'initial': None})
        self.fields['roles'].required = True


class OrganizationInvitationAdminForm(forms.ModelForm):
    class Meta:
        model = OrganizationInvitation
        fields = '__all__'
