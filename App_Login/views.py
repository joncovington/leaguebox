from django.shortcuts import render, HttpResponseRedirect
from django.urls import reverse
from django.utils.http import is_safe_url
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, logout, authenticate


from allauth.account.views import SignupView

# Messages
from django.contrib import messages

from App_Login.models import Profile
from App_Login.forms import ProfileForm, SignUpForm


class PageTitleMixin(object):
    def get_page_title(self, context):
        return getattr(self, "page_title", "Default Page Title")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = self.get_page_title(context)

        return context


class AccountSignupView(SignupView):
    template_name = "App_Login/custom_signup.html"


def sign_up(request):
    form = SignUpForm()
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Account Created Successfully")
            return HttpResponseRedirect(reverse('App_Login:login'))
    return render(request, 'App_Login/sign_up.html', context={'title': 'Sign Up', 'form': form})


def log_in(request):

    form = AuthenticationForm()
    if request.method == 'POST':
        nxt = request.POST.get('next')
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                if not is_safe_url(url=nxt, allowed_hosts=request.get_host()):
                    return HttpResponseRedirect(reverse('App_Organization:home'))
                else:
                    return HttpResponseRedirect(nxt)
    return render(request, 'App_Login/login.html', context={'page_title': 'Log In', 'form': form})


@login_required
def log_out(request):
    logout(request)
    messages.warning(request, "You are logged out.")
    return HttpResponseRedirect(reverse('App_Organization:home'))


@login_required
def user_profile(request):
    profile = Profile.objects.get(user=request.user)

    form = ProfileForm(instance=profile)
    if request.method == 'POST':
        form = ProfileForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            messages.success(request, "Profile Updated Successfully")
            form = ProfileForm(instance=profile)
            return HttpResponseRedirect(reverse('App_Organization:home'))
    return render(request, 'App_Login/change_profile.html', context={'title': 'Profile', 'form': form})
