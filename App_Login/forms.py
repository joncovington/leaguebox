from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm

from App_Login.models import User, Profile


class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        exclude = (
            'user',
            'date_joined'
        )
    display_name = forms.CharField(
        label='Username (Required)',
        widget=forms.TextInput(attrs={'placeholder': 'Enter Your Username/Alias Here'})
    )

    first_name = forms.CharField(
        label='First Name (Required)',
        widget=forms.TextInput(attrs={'placeholder': 'Enter Your First Name Here'})
    )

    last_name = forms.CharField(
        label='Last Name (Required)',
        widget=forms.TextInput(attrs={'placeholder': 'Enter Your Last Name Here'})
    )


class SignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = (
            'email',
            'password1',
            'password2'
        )
