from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.utils.translation import ugettext_lazy

# to automatically create one to one object
from django.db.models.signals import post_save
from django.dispatch import receiver


class MyUserManager(BaseUserManager):

    def _create_user(self, email, password, **extra_fields):

        if not email:
            raise ValueError('The Email must be set')

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault(
            'is_staff', True
        )
        extra_fields.setdefault(
            'is_superuser', True
        )
        extra_fields.setdefault(
            'is_active', True
        )

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True')

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True')

        if extra_fields.get('is_active') is not True:
            raise ValueError('Superuser must have is_active=True')
        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField(unique=True, null=False)
    is_staff = models.BooleanField(
        ugettext_lazy('Staff Status'),
        default=False,
        help_text=ugettext_lazy('Designates if user is staff')
    )
    is_active = models.BooleanField(
        ugettext_lazy('Account Active Status'),
        default=True,
        help_text=ugettext_lazy('Designates if user is active')
    )

    USERNAME_FIELD = 'email'
    objects = MyUserManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    display_name = models.CharField(max_length=80, null=True)
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    address_1 = models.TextField(max_length=300, null=True, blank=True)
    address_2 = models.TextField(max_length=300, null=True, blank=True)
    city = models.CharField(max_length=40, null=True, blank=True)
    state = models.CharField(max_length=2, null=True, blank=True)
    zipcode = models.CharField(max_length=10, null=True, blank=True)
    country = models.CharField(max_length=20, null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    date_joined = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{str(self.user.email)} | {str(self.display_name)}'

    def is_fully_filled(self):
        # fields_name = [f.name for f in self._meta.get_fields()]
        fields_name = ['display_name', 'first_name', 'last_name']
        for field_name in fields_name:
            value = getattr(self, field_name)
            if value is None or value == '':
                return False
        return True

    def get_profile_firstlast(self):
        return f'{self.first_name} {self.last_name}'


@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_profile(sender, instance, **kwargs):
    instance.profile.save()
