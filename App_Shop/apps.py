from django.apps import AppConfig


class AppShopConfig(AppConfig):
    name = 'App_Shop'

    def ready(self):
        import App_Shop.signals