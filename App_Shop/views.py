import time
from django.shortcuts import redirect
from django.utils import timezone
import stripe
import json
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.conf import settings
from django.http.response import HttpResponseRedirect, JsonResponse, HttpResponse
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView, View
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView

from App_Organization.decorators import check_org_admin, user_is_org_admin

from App_Shop.forms import NewProductForm

from App_Organization.models import Organization
from App_Shop.models import CartItem, EventCart, Product, ProductImage


@method_decorator(user_is_org_admin, name='dispatch')
class NewProductView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    form_class = NewProductForm
    template_name = 'App_Shop/new_product.html'
    success_url = '/'
    success_message = 'Product has been created.'

    def dispatch(self, request, *args, **kwargs):
        self.organization = Organization.objects.get(slug=kwargs['slug'])
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.organization = self.organization

        return super().form_valid(form)


@method_decorator(user_is_org_admin, name='dispatch')
class NewProductImage(LoginRequiredMixin, TemplateView):
    template_name = 'App_Shop/new_product_images.html'

    def get_context_data(self, **kwargs):
        self.context = super(NewProductImage, self).get_context_data(**kwargs)
        product_id = self.context['pk']
        product = Product.objects.get(pk=product_id)
        kwargs['product'] = product
        return super().get_context_data(**kwargs)


@method_decorator(user_is_org_admin, name='dispatch')
class ImageUpload(LoginRequiredMixin, View):

    def post(self, request, slug, pk):
        product = Product.objects.get(pk=pk)
        images = request.FILES.get('file[0]', None)
        if images:
            files = [request.FILES.get('file[%d]' % i) for i in range(0, len(request.FILES))]
            for img in files:
                print(img.name)
                ProductImage(product=product, image=img).save()
            messages.add_message(self.request, messages.SUCCESS, 'Image(s) Added Successfully')
        return HttpResponseRedirect(reverse('App_Organization:organization', kwargs={'slug': slug}))


@method_decorator(user_is_org_admin, name='dispatch')
class ProductListView(LoginRequiredMixin, ListView):
    model = Product
    context_object_name = 'products'
    template_name = 'App_Shop/product_list.html'

    def dispatch(self, request, *args, **kwargs):
        self.organization = Organization.objects.get(slug=kwargs['slug'])
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return Product.objects.filter(organization=self.organization)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = f'{self.organization} Products'
        context['organization'] = self.organization
        return context


class ProductDetail(LoginRequiredMixin, DetailView):
    model = Product
    pk_url_kwarg = 'pk'

    def dispatch(self, request, *args, **kwargs):
        self.organization = Organization.objects.get(slug=kwargs['slug'])
        self.product = Product.objects.get(pk=kwargs['pk'])
        self.admin = check_org_admin(get_roles=False, slug=self.organization.slug, user=request.user)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['admin'] = self.admin
        context['page_title'] = f'{self.product.name}'
        return context


class CreateInvoice(View):

    def get(self, request, *args, **kwargs):

        stripe.api_key = settings.STRIPE_SECRET_KEY
        cart_id = self.kwargs["pk"]
        event_cart = EventCart.objects.get(cart=cart_id)
        event_venue_or_user = ''
        if event_cart.event.user_org:
            event_venue_or_user = event_cart.event.user_org
        else:
            event_venue_or_user = event_cart.event.user
        total = int(float(event_cart.cart.get_cart_total())*100)
        application_fee = 0.01 * float(event_cart.cart.fee())
        # print(int(application_fee * total))

        transfer_data = {'destination': event_cart.event.organization.league.stripe_user_id, }

        metadata = {
            "event_cart_id": event_cart.id,
            "cart_id": cart_id,
            "event_date": 'Event Date: ' + str(event_cart.event.start),
            "event_id": event_cart.event.id,
            "event_venue": event_venue_or_user
        }
        customer = get_or_create_customer(
            email=request.user.email
        )
        for item in CartItem.objects.filter(cart=event_cart.cart):
            ii = stripe.InvoiceItem.create(
                customer=customer['id'],
                price=item.product.stripe_price_id,
                quantity=item.quantity
            )
            print(ii)
        # create or retrieve customer in stripe
        event_date = event_cart.event.start
        due_date_timestamp = time.mktime(event_date.timetuple())

        invoice = stripe.Invoice.create(
            customer=customer['id'],
            collection_method='send_invoice',
            due_date=int(due_date_timestamp),
            metadata=metadata,
            application_fee_amount=int(total * application_fee),
            transfer_data=transfer_data
        )
        stripe.Invoice.finalize_invoice(
            invoice['id'],
        )
        event_cart.stripe_invoice_id = invoice['id']
        event_cart.save()
        return HttpResponseRedirect(reverse('App_Organization:home'))


class CheckoutSuccess(View):

    def get(self, request, *args, **kwargs):
        messages.add_message(self.request, messages.SUCCESS, 'Checkout Successful')
        return HttpResponseRedirect(reverse('App_Organization:home'))


class CheckoutCancel(View):

    def get(self, request, *args, **kwargs):
        messages.add_message(self.request, messages.ERROR, 'Checkout Cancelled')
        return HttpResponseRedirect(reverse('App_Organization:home'))


class GetPaymentIntent(View):

    def post(self, request, *args, **kwargs):
        try:
            req_json = json.loads(request.body)
            print(req_json)
            stripe.api_key = settings.STRIPE_SECRET_KEY
            customer = get_or_create_customer(
                email=req_json['email'],
                name=req_json['first'] + ' ' + req_json['last'],
                phone=req_json['phone']
                )
            cart_id = self.kwargs["pk"]
            # print(customer)
            event_cart = EventCart.objects.get(cart=cart_id)
            event_venue_or_user = ""
            if event_cart.event.user_org:
                event_venue_or_user = event_cart.event.user_org
            else:
                event_venue_or_user = event_cart.event.user
            total = int(float(event_cart.cart.get_cart_total())*100)
            application_fee = 0.01 * float(event_cart.cart.fee())
            # print(int(application_fee * total))
            intent = stripe.PaymentIntent.create(
                amount=total,
                currency='usd',
                customer=customer['id'],
                metadata={
                    "cart_id": cart_id,
                    "event_date": 'Event Date: ' + str(event_cart.event.start),
                    "event_id": event_cart.event.id,
                    "event_venue": event_venue_or_user
                },
                transfer_data={
                    'destination': event_cart.event.organization.league.stripe_user_id,
                },
                application_fee_amount=int(total * application_fee),
                payment_method_types=['card'],
            )
            print(intent)
            return JsonResponse({
                'clientSecret': intent['client_secret']
            })
        except Exception as e:
            return JsonResponse({'error': str(e)})


def get_or_create_customer(email):
    stripe.api_key = settings.STRIPE_SECRET_KEY
    connected_customers = stripe.Customer.list()
    for customer in connected_customers:
        if customer.email == email:
            print(f'{email} found')
            return customer
    print(f'{email} created')
    return stripe.Customer.create(
            email=email,
       )


@csrf_exempt
def stripe_webhook(request):
    stripe.api_key = settings.STRIPE_SECRET_KEY
    endpoint_secret = settings.STRIPE_WEBHOOK_SECRET
    payload = request.body
    sig_header = request.META['HTTP_STRIPE_SIGNATURE']
    event = None

    try:
        event = stripe.Webhook.construct_event(
            payload, sig_header, endpoint_secret
        )
    except ValueError as e:
        # Invalid payload
        return HttpResponse(status=400)
    except stripe.error.SignatureVerificationError as e:
        # Invalid signature
        return HttpResponse(status=400)

    print(event['type'])
    # handle the invoice.paid event from stripe
    if event['type'] == 'invoice.paid':

        session = event['data']['object']
        invoice_id = session['id']
        invoice = stripe.Invoice.retrieve(
            invoice_id,
        )
        print(invoice_id)
        # get metadata event cart
        if invoice['metadata']:
            metadata = invoice['metadata']
            event_cart_id = metadata['event_cart_id']
            event_cart = EventCart.objects.get(pk=event_cart_id)
            event_cart.cart.purchased = True
            event_cart.cart.purchased_date = timezone.now()
            event_cart.cart.stripe_payment_id = session["payment_intent"]
            event_cart.cart.stripe_customer_id = session["customer"]
            event_cart.cart.save()
            event_cart.event.submitted = True
            event_cart.event.submitted_date = timezone.now()
            event_cart.event.save()

    return HttpResponse(status=200)


class CartPayment(LoginRequiredMixin, TemplateView):
    template_name = 'App_Shop/cart_payment.html'

    def dispatch(self, request, *args, **kwargs):
        self.organization = Organization.objects.get(slug=kwargs['slug'])
        self.event_cart = EventCart.objects.get(cart=kwargs['cart'])
        self.user = request.user
        self.admin = check_org_admin(get_roles=False, slug=kwargs['slug'], user=request.user)

        # Check if user is the event admin
        self.event_admin = False
        if self.user.email == self.event_cart.event.user.email:
            self.event_admin = True
        else:
            self.event_admin = check_org_admin(get_roles=False, slug=self.event_cart.event.user_org.slug, user=self.user)
        if not self.event_admin:
            messages.add_message(request, messages.ERROR, 'You do not have permissions to perform this action.')
            return redirect(reverse('App_Organization:home'))
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CartPayment, self).get_context_data(**kwargs)
        context['event_admin'] = self.event_admin

        context['cart'] = self.event_cart.cart
        context['event'] = self.event_cart.event
        context['admin'] = self.admin
        context['successURL'] = reverse('App_Organization:event_submit', kwargs={'slug': self.organization.slug, 'pk': self.event_cart.event.pk})
        context['key'] = settings.STRIPE_PUBLISHABLE_KEY
        return context
