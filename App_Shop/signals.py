import stripe
from django.db.models import signals
from django.dispatch import receiver
from App_Shop.models import Product


@receiver(signals.post_save, sender=Product)
def update_stripe_product(sender, instance, created, **kwargs):
    if created and instance.stripe_product_id:
        updated_product = stripe.Product.modify(
                        instance.stripe_product_id,
                        metadata={
                            'product_id': instance.pk,
                            'category': instance.category,
                            'requires_role': instance.role,
                            'application_unit': instance.unit,
                            'organization': instance.organization
                        },
                        )
