import stripe
import os
from PIL import Image
from io import BytesIO
from django.core.files.base import ContentFile
from django.conf import settings
from django.urls import reverse
from django.db import models
from django.template.defaultfilters import slugify

from App_Organization.models import Organization, Role

from App_Event.models import Event


class ProductType(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.name}"


class Category(models.Model):
    name = models.CharField(max_length=20)

    class Meta:
        verbose_name_plural = "categories"

    def __str__(self):
        return f"{self.name}"


class ProductUnit(models.Model):
    name = models.CharField(max_length=20)
    short_name = models.CharField(max_length=5)

    def __str__(self):
        return f"{self.name}"


class Product(models.Model):
    product_type = models.ForeignKey(ProductType, on_delete=models.DO_NOTHING, related_name='product_type')
    category = models.ForeignKey(Category, on_delete=models.DO_NOTHING, related_name='product_category', null=True, blank=True)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, related_name='product_org')
    name = models.CharField(max_length=80)
    slug = models.SlugField()
    short_description = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    unit = models.ForeignKey(ProductUnit, on_delete=models.DO_NOTHING, related_name='product_unit')
    price = models.DecimalField(max_digits=7, decimal_places=2)
    default_seats = models.SmallIntegerField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)
    expires = models.DateField(null=True, blank=True, help_text='Optional. After this date, the product will no longer be available to purchase.')
    role = models.ForeignKey(Role, on_delete=models.DO_NOTHING, null=True, blank=True)
    stripe_product_id = models.CharField(max_length=255, blank=True, null=True)
    stripe_price_id = models.CharField(max_length=255, blank=True, null=True)
    stripe_recurring_price_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        ordering = ['organization__organization_name', 'name']
        unique_together = ['name', 'organization']

    def __str__(self):
        return f'{self.name} | {self.organization.organization_name}'

    def save(self, *args, **kwargs):
        if not self.slug:
            slug_str = self.organization.organization_name + ' ' + self.name
            self.slug = slugify(slug_str)
        # Add product to Stripe
        if not self.stripe_product_id:
            stripe.api_key = settings.STRIPE_SECRET_KEY
            stripe_product = stripe.Product.create(
                name=self.name,
                description=self.short_description,
                )
            self.stripe_product_id = stripe_product['id']
        if not self.stripe_price_id:
            stripe_price = stripe.Price.create(
                product=self.stripe_product_id,
                unit_amount=int(self.price*100),
                currency='usd',
            )
            self.stripe_price_id = stripe_price['id']
        if not self.stripe_recurring_price_id:
            stripe_recurring_price = stripe.Price.create(
                product=self.stripe_product_id,
                unit_amount=int(self.price*100),
                currency='usd',
                recurring={
                    'interval': 'week',
                },
            )
            self.stripe_recurring_price_id = stripe_recurring_price['id']
        print(self.stripe_product_id)
        print(self.stripe_price_id)
        print(self.stripe_recurring_price_id)
        return super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('product', kwargs={'slug': self.slug})


class ProductImage(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='image_product')
    order = models.FloatField(default=1.0)
    image = models.ImageField(upload_to='product_images/')
    thumbnail = models.ImageField(upload_to='product_images/thumbs/', editable=False, null=True, blank=True)

    class Meta:
        ordering = ['product__name', '-order']

    def save(self, *args, **kwargs):
        print(os.path.splitext(self.image.name))
        thumbnail = self.make_thumbnail()
        print(thumbnail)
        super(ProductImage, self).save(*args, **kwargs)

    def make_thumbnail(self):
        thumb_name, thumb_extension = os.path.splitext(self.image.name)
        thumb_extension = thumb_extension.lower()
        image = Image.open(self.image)

        image.thumbnail(settings.THUMB_SIZE, Image.ANTIALIAS)
        thumb_filename = thumb_name + '_thumb' + thumb_extension

        if thumb_extension in ['.jpg', '.jpeg']:
            FTYPE = 'JPEG'
        elif thumb_extension == '.gif':
            FTYPE = 'GIF'
        elif thumb_extension == '.png':
            FTYPE = 'PNG'
        else:
            return False  # Unrecognized file type

        # Save thumbnail to in-memory file as StringIO
        temp_thumb = BytesIO()
        image.save(temp_thumb, FTYPE, quality=70)
        temp_thumb.seek(0)

        self.thumbnail.save(thumb_filename, ContentFile(temp_thumb.read()), save=False)

        temp_thumb.close()

        return True

    def __str__(self):
        return f"{self.product.name} {self.product.id} {self.order}"


class ProductTag(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='tag_product')
    tag = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.product.name} {self.tag}"


class Cart(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING, related_name='cart_user')
    organization = models.ForeignKey(Organization, on_delete=models.DO_NOTHING, related_name='cart_org')
    purchased = models.BooleanField(default=False)
    purchased_date = models.DateTimeField(blank=True, null=True)
    stripe_payment_id = models.CharField(max_length=255, null=True, blank=True)
    stripe_customer_id = models.CharField(max_length=255, null=True, blank=True)

    def get_cart_total(self):
        return cart_total(self)

    def fee(self):
        return 6


class CartItem(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.DO_NOTHING, related_name='cart_items')
    product = models.ForeignKey(Product, on_delete=models.DO_NOTHING, related_name='cart_product')
    quantity = models.PositiveSmallIntegerField()


class EventCart(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.DO_NOTHING, related_name='cart')
    event = models.ForeignKey(Event, on_delete=models.DO_NOTHING, related_name='event_cart')
    stripe_invoice_id = models.CharField(max_length=255, null=True, blank=True)

    def get_cart_items(self):
        cart_items = CartItem.objects.filter(cart=self.cart)
        return cart_items


def cart_total(cart):
    cart_items = CartItem.objects.filter(cart=cart)
    total = 0.00
    if cart_items:
        for item in cart_items:
            total += float(item.quantity) * float(item.product.price)
        return format(total, '.2f')
    else:
        return total
