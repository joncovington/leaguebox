from django.urls import path
from App_Shop import views

app_name = 'App_Shop'

urlpatterns = [
    path('<slug>/<pk>/new-images/', views.NewProductImage.as_view(), name='new_images'),
    path('<slug>/<pk>/new-images/upload/', views.ImageUpload.as_view(), name='upload'),
]
