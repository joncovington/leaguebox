from django import forms
from django.forms.widgets import DateInput
from App_Shop.models import Product


class NewProductForm(forms.ModelForm):
    class Meta:
        model = Product
        exclude = ['organization', 'slug', 'active', 'stripe_product_id', 'stripe_price_id', 'stripe_recurring_price_id']

    def __init__(self, *args, **kwargs):
        super(NewProductForm, self).__init__(*args, **kwargs)
        self.fields['expires'].widget = DateInput(attrs={
            'type': 'date'
            })
