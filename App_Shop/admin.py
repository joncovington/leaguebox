from django.contrib import admin
from App_Shop.models import Cart, CartItem, EventCart, Product, ProductUnit, ProductType, ProductImage, ProductTag, Category
# Register your models here.

admin.site.register(Product)
admin.site.register(ProductUnit)
admin.site.register(ProductType)
admin.site.register(ProductImage)
admin.site.register(ProductTag)
admin.site.register(Category)
admin.site.register(Cart)
admin.site.register(CartItem)
admin.site.register(EventCart)
