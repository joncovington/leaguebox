# Generated by Django 3.1.5 on 2021-03-14 16:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App_Shop', '0015_auto_20210313_1039'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='stripe_product_id',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
