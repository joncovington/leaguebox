# Generated by Django 3.1.5 on 2021-02-17 23:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App_Shop', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='productunit',
            name='short_name',
            field=models.CharField(default='none', max_length=5),
            preserve_default=False,
        ),
    ]
