from django.urls import path
from App_Event import views

app_name = 'App_Event'

urlpatterns = [
    path('<pk>/init/', views.EventInit.as_view(), name='initialize'),
    path('<pk>/subevent/<int:subevent>/config/', views.SubeventConfig.as_view(), name='config'),
    path('subevent/open/', views.OpenRegistration.as_view(), name='open'),
    path('subevent/<pk>/reg-toggle/', views.AllowSubeventRegistrationToggle.as_view(), name='allow_reg_toggle'),
    path('subevent/<pk>/review/', views.SubeventReview.as_view(), name='sub_review'),
    path('subevent/<int:pk>/review/<str:tab>/', views.SubeventReview.as_view(), name='sub_review_tab'),
    path('subevent/<pk>/reg/', views.RegisterToggle.as_view(), name='reg_toggle'),
    path('add-subevent/', views.AddSubevent.as_view(), name='add_subevent'),
    path('subevent/<pk>/finalize/', views.FinalizeSubeventStart.as_view(), name='sub_finalize'),
    path('get-rank-structure', views.GetRankStructure.as_view(), name='get_rank_structure'),
    path('subevent/<int:pk>/<int:rank_set>/process/', views.ProcessRanking.as_view(), name='process'),
]
