import json
import re
from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.utils import timezone
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import HttpResponse, HttpResponseRedirect
from django.urls.base import reverse
from django.views.generic.base import TemplateView, View
from django.views.generic.edit import FormView
from django.utils.decorators import method_decorator
from django.db.models import Q

# imports for email
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.contrib.sites.shortcuts import get_current_site
import stripe

from App_Event.decorators import is_event_user

from App_Event.models import Event, EventDetail, EventStaff, SubEvent, SubEventRegistration, SubEventType
from App_Event.forms import NewEventForm, NewEventDetailForm, SubeventConfigForm

from App_Organization.models import Organization, Role, Staff
from App_Organization.decorators import check_org_admin, user_is_org_admin
from App_Organization.views import check_roles

from App_Shop.models import Product, Cart, CartItem, EventCart

from App_Post.models import Post

from App_Game.models import GameEquip, GameRank, GameSeat, Game, RankRange, RankSet, RankStructure
from App_Equipment.models import Equipment


class EventSubmittedMixin(object):
    '''Checks Event submitted status in model. Redirects home if Event model submitted bool is True'''
    def dispatch(self, request, *args, **kwargs):
        if 'subevent' in request.path:
            subevent = SubEvent.objects.get(pk=kwargs['pk'])
            self.event = subevent.event
        else:
            self.event = Event.objects.get(pk=kwargs['pk'])
        self.organization = self.event.organization
        self.user = request.user
        if self.event.submitted:
            messages.add_message(request, messages.WARNING, 'Event has already been submitted.')
            return redirect(reverse('App_Organization:home'))
        return super().dispatch(request, *args, **kwargs)


class IsEventStaffMixin(object):
    '''Checks that user is in event staff and creates a context flag for is_event_staff'''
    def dispatch(self, request, *args, **kwargs):
        pk = kwargs['pk']
        subevent_flag = False
        if 'subevent' in request.path:
            subevent_flag = True
            subevent = SubEvent.objects.get(pk=pk)
            self.event = subevent.event
        elif 'seat' in request.path:
            seat = GameSeat.objects.get(pk=pk)
            self.event = seat.game_equip.game.subevent.event
        elif ('start' in request.path) and ('games' in request.path):
            game = Game.objects.get(pk=pk)
            self.event = game.subevent.event
        elif 'section' in request.path:
            equip = GameEquip.objects.get(pk=pk)
            self.event = equip.game.subevent.event
        else:
            self.event = Event.objects.get(pk=pk)

        self.event_staff = EventStaff.objects.filter(event=self.event)
        if self.request.user.id not in self.event_staff.values_list('user', flat=True):
            messages.add_message(request, messages.ERROR, 'You do not have permissions to perform this action')
            if subevent_flag:
                return HttpResponseRedirect(reverse('App_Event:sub_review', kwargs={'pk': pk}))
            else:
                return HttpResponseRedirect(reverse('App_Organization:event_org_review', kwargs={'slug': self.event.organization.slug, 'pk': self.event.pk}))

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.id in self.event_staff.values_list('user', flat=True):
            context["is_event_staff"] = True
        return context


class NewEvent(LoginRequiredMixin, FormView):
    model = Event
    form_class = NewEventForm
    template_name = 'App_Event/new_event_dialog_start.html'

    def dispatch(self, request, *args, **kwargs):
        self.organization = Organization.objects.get(slug=kwargs['slug'])
        self.user = request.user

        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(NewEvent, self).get_form_kwargs()
        kwargs.update({'user': self.request.user, 'organization': self.organization})
        return kwargs

    def form_valid(self, form):
        new_event = Event(
            user=self.user,
            organization=self.organization,
            user_org=form.cleaned_data['user_org'],
            start=form.cleaned_data['start'],
            end=form.cleaned_data['end']
        )
        new_event.save()
        self.event = new_event
        return super(NewEvent, self).form_valid(form)

    def get_success_url(self):
        return reverse('App_Organization:new_event_detail', kwargs={'slug': self.organization.slug, 'pk': self.event.pk})

    def get_context_data(self, **kwargs):
        context = super(NewEvent, self).get_context_data(**kwargs)
        context['organization'] = self.organization
        return context


@method_decorator(is_event_user, name='dispatch')
class NewEventDetail(EventSubmittedMixin, LoginRequiredMixin, FormView):
    model = EventDetail
    form_class = NewEventDetailForm
    template_name = 'App_Event/new_event_dialog_detail.html'

    def get_form_kwargs(self):
        kwargs = super(NewEventDetail, self).get_form_kwargs()
        kwargs.update({'user_org': self.event.user_org, 'event': self.event})
        return kwargs

    def form_valid(self, form):
        if EventDetail.objects.filter(event=self.event).exists():
            self.event_detail = EventDetail.objects.get(event=self.event)
            self.event_detail.address_1 = form.cleaned_data['address_1']
            self.event_detail.address_2 = form.cleaned_data['address_2']
            self.event_detail.city = form.cleaned_data['city']
            self.event_detail.state = form.cleaned_data['state']
            self.event_detail.postal_code = form.cleaned_data['postal_code']
            self.event_detail.phone = form.cleaned_data['phone']
            self.event_detail.requests = form.cleaned_data['requests']
            self.event_detail.save()
        else:
            new_event_detail = EventDetail(
                event=self.event,
                address_1=form.cleaned_data['address_1'],
                address_2=form.cleaned_data['address_2'],
                city=form.cleaned_data['city'],
                state=form.cleaned_data['state'],
                postal_code=form.cleaned_data['postal_code'],
                phone=form.cleaned_data['phone'],
                requests=form.cleaned_data['requests'],
            )
            new_event_detail.save()
            self.event_detail = new_event_detail
        return super(NewEventDetail, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(NewEventDetail, self).get_context_data(**kwargs)
        context['organization'] = self.organization
        return context

    def get_success_url(self):
        return reverse('App_Organization:new_event_cart', kwargs={'slug': self.organization.slug, 'pk': self.event.pk})


def get_cart_total(cart):
    cart_items = CartItem.objects.filter(cart=cart)
    total = 0.00
    if cart_items:
        for item in cart_items:
            total += float(item.quantity) * float(item.product.price)
        return format(total, '.2f')
    else:
        return total


@method_decorator(is_event_user, name='dispatch')
class BuildEventCart(EventSubmittedMixin, LoginRequiredMixin, TemplateView):
    template_name = 'App_Event/new_event_build_cart.html'

    def get_context_data(self, **kwargs):

        context = super(BuildEventCart, self).get_context_data(**kwargs)
        context['products'] = Product.objects.filter(organization=self.organization, product_type__name='Event').order_by('name')
        context['organization'] = self.organization
        context['event'] = self.event

        cart_check = Cart.objects.filter(user=self.user, organization=self.organization)
        if cart_check:
            cart = Cart.objects.get(user=self.user, organization=self.organization)
            # does this event already have an EventCart?
            event_cart_check = EventCart.objects.filter(event=self.event, cart=cart)
            if event_cart_check:
                event_cart = EventCart.objects.get(event=self.event, cart=cart)
                context['cart_total'] = get_cart_total(event_cart.cart)

            # Pass in cart items if exist
            cart_items_check = CartItem.objects.filter(cart=cart)
            if cart_items_check:
                context['cart_items'] = cart_items_check
            else:
                context['cart_items'] = None
        else:
            context['cart_total'] = 0.00
        return context


@method_decorator(is_event_user, name='dispatch')
class AddToEventCart(LoginRequiredMixin, View):

    def post(self, request, slug, pk):
        if request.POST:
            organization = Organization.objects.get(slug=slug)
            event = Event.objects.get(pk=pk)
            product = Product.objects.get(pk=request.POST['product_id'])

            cart_check = Cart.objects.filter(user=request.user, organization=organization)

            if cart_check:
                cart = Cart.objects.get(user=request.user, organization=organization)
            else:
                cart = Cart(
                    user=request.user,
                    organization=organization
                )
                cart.save()

            event_cart_check = EventCart.objects.filter(cart=cart, event=event)
            if event_cart_check:
                event_cart = EventCart.objects.get(cart=cart, event=event)
            else:
                event_cart = EventCart(
                    cart=cart,
                    event=event
                )
                event_cart.save()

            response_data = {}

            # Does product already exist in cart?
            cart_item = CartItem.objects.filter(cart=cart, product=product)
            if cart_item:
                cart_item[0].quantity += 1
                cart_item[0].save()
                response_data['product_qty'] = cart_item[0].quantity
                response_data['product_name'] = cart_item[0].product.name
            else:
                new_cart_item = CartItem(
                    cart=cart,
                    product=product,
                    quantity=1
                )
                new_cart_item.save()
                response_data['product_qty'] = new_cart_item.quantity

            response_data['cart_total'] = get_cart_total(cart)
            return HttpResponse(
                json.dumps(response_data),
                content_type="application/json"
                )
        else:
            return HttpResponse(
                json.dumps({"nothing to see": "this isn't happening"}),
                content_type="application/json"
            )


@method_decorator(is_event_user, name='dispatch')
class RemoveFromEventCart(LoginRequiredMixin, View):

    def post(self, request, slug, pk):
        if request.POST:
            organization = Organization.objects.get(slug=slug)
            event = Event.objects.get(pk=pk)
            product = Product.objects.get(pk=request.POST['product_id'])

            cart_check = Cart.objects.filter(user=request.user, organization=organization)

            if cart_check:
                cart = Cart.objects.get(user=request.user, organization=organization)
            else:
                cart = Cart(
                    user=request.user,
                    organization=organization
                )
                cart.save()

            event_cart_check = EventCart.objects.filter(cart=cart, event=event)
            if event_cart_check:
                event_cart = EventCart.objects.get(cart=cart, event=event)
            else:
                event_cart = EventCart(
                    cart=cart,
                    event=event
                )
                event_cart.save()

            response_data = {}

            # Does product already exist in cart?
            cart_items = CartItem.objects.filter(cart=cart, product=product)
            if cart_items:
                cart_item = CartItem.objects.get(cart=cart, product=product)

                if cart_item.quantity >= 1:
                    cart_item.quantity -= 1
                    cart_item.save()
                    response_data['product_qty'] = cart_item.quantity
                if cart_item.quantity == 0:
                    cart_item.delete()
                    response_data['product_qty'] = 0

            response_data['cart_total'] = get_cart_total(cart)
            return HttpResponse(
                json.dumps(response_data),
                content_type="application/json"
                )
        else:
            return HttpResponse(
                json.dumps({"nothing to see": "this isn't happening"}),
                content_type="application/json"
            )


@method_decorator(is_event_user, name='dispatch')
class EventReview(EventSubmittedMixin, LoginRequiredMixin, TemplateView):
    template_name = 'App_Event/new_event_review.html'

    def get_context_data(self, **kwargs):
        context = super(EventReview, self).get_context_data(**kwargs)
        if EventDetail.objects.filter(event=self.event).exists():
            context['event_details'] = EventDetail.objects.get(event=self.event)
        context['event'] = self.event
        event_carts = EventCart.objects.filter(event=self.event)
        carts = Cart.objects.filter(cart__in=event_carts)
        context['carts'] = carts
        context['key'] = settings.STRIPE_PUBLISHABLE_KEY
        return context


@method_decorator(is_event_user, name='dispatch')
class EventFinalSubmit(EventSubmittedMixin, LoginRequiredMixin, View):

    def get(self, request, slug, pk):
        self.organization = Organization.objects.get(slug=slug)
        self.event = Event.objects.get(pk=pk)
        self.user = request.user

        # Do stuff to mark event submitted, email user and event organization admins

        self.event.submitted = True
        self.event.submitted_date = timezone.now()
        self.event.save()

        # get all admin emails from servicing organization
        email_list = []
        org_staff = Staff.objects.filter(organization=self.organization)
        for person in org_staff:
            for role in person.roles.all():
                if role.role == 'Admin':
                    email_list.append(person.user.email)
        # add event user to email list
        email_list.append(self.event.user.email)

        # get templates and build email
        current_site = get_current_site(request)
        plaintext_email = get_template('App_Event/email/new_event.txt')
        html_email = get_template('App_Event/email/new_event.html')
        email_context = {
            'event': self.event,
            'site': reverse('App_Organization:home'),
            'signup': reverse('App_Login:signup'),
            'domain': current_site
        }
        subject, from_email, to_email = 'New Event Submitted!', settings.NO_REPLY_EMAIL, email_list
        text_content = plaintext_email.render(email_context)
        html_content = html_email.render(email_context)
        msg = EmailMultiAlternatives(subject, text_content, from_email, to_email)
        msg.attach_alternative(html_content, "text/html")
        msg.send()

        messages.add_message(self.request, messages.SUCCESS, 'Event Submitted Successfully')

        return HttpResponseRedirect(reverse('App_Organization:home'))


class UserEvents(LoginRequiredMixin, TemplateView):
    template_name = 'App_Event/my_events.html'

    def get_context_data(self, **kwargs):
        context = super(UserEvents, self).get_context_data(**kwargs)
        self.user_events = Event.objects.filter(user=self.request.user)
        if self.user_events.exists():
            context['user_events'] = self.user_events
        return context


def get_available_equip(event):
    '''Using given event, return equipment that is not currently assigned to a subevent'''
    if event.user_org:
        # Get equipment that is at event location and is NOT already assigned to a game
        game_equip_qs = GameEquip.objects.all().values_list('equip', flat=True)
        results = Equipment.objects.exclude(id__in=game_equip_qs).filter(organization=event.organization, location=event.user_org)
        return results
    else:
        # Get equipment that is stored for private game and is NOT already assigned to a game
        game_equip_qs = GameEquip.objects.all().values_list('equip', flat=True)
        results = Equipment.objects.exclude(id__in=game_equip_qs).filter(organization=event.organization, location=event.organization)
        return results


class EventOrgReview(LoginRequiredMixin, TemplateView):
    template_name = "App_Event/event_review.html"

    def dispatch(self, request, *args, **kwargs):
        self.event = Event.objects.get(pk=kwargs['pk'])
        self.organization = self.event.organization
        self.admin = check_org_admin(get_roles=False, slug=kwargs['slug'], user=request.user)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EventOrgReview, self).get_context_data(**kwargs)

        # Check if user is the event admin
        self.event_admin = False
        if self.request.user.email == self.event.user.email:
            self.event_admin = True
        else:
            self.event_admin = check_org_admin(get_roles=False, slug=self.event.user_org.slug, user=self.request.user)
        context['event_admin'] = self.event_admin
        context['event'] = self.event
        context['admin'] = self.admin

        # get event cart information for context
        event_carts = EventCart.objects.filter(event=self.event)
        context['event_carts'] = event_carts
        # carts = Cart.objects.filter(cart__in=event_carts)
        # context['carts'] = carts

        # get stripe invoice for event carts
        stripe.api_key = settings.STRIPE_SECRET_KEY
        invoices = []
        for event_cart in event_carts:
            if event_cart.stripe_invoice_id:
                stripe_invoice = stripe.Invoice.retrieve(
                    event_cart.stripe_invoice_id
                )
                invoices.append(
                    {
                        'event_cart_id': event_cart.id,
                        'url': stripe_invoice['hosted_invoice_url'],
                        'status': stripe_invoice['status'],
                    }
                )
        context['invoices'] = invoices

        # get event staff for context

        user_roles, user_admin = check_roles(self.organization, self.request.user)
        user_roles_list = []
        for (org_type, role) in user_roles:
            user_roles_list.append(f'{org_type} {role}')
        event_staff_qs = EventStaff.objects.filter(event=self.event)
        staff_spot_list = []
        context['is_event_staff'] = False
        for staff_spot in event_staff_qs:
            if staff_spot.user == self.request.user:
                context['is_event_staff'] = True
            for role in user_roles_list:
                if role == str(staff_spot.product.role):
                    staff_spot_list.append(staff_spot)
        context['event_staff_spots'] = staff_spot_list
        context['event_staff'] = event_staff_qs

        # check for allow_reg to enable registration tab
        context['allow_reg'] = context['is_event_staff']
        subevents = SubEvent.objects.filter(event=self.event)
        for subevent in subevents:
            if subevent.allow_reg:
                context['allow_reg'] = True

        # use function to get available tables for new subevents
        open_equip = get_available_equip(self.event)
        print(open_equip)
        if open_equip:
            context['open_equip'] = open_equip

        context['key'] = settings.STRIPE_PUBLISHABLE_KEY
        context['allow_init'] = timezone.localtime(timezone.now()).date() == timezone.localtime(self.event.start).date()

        return context


class EventApprove(LoginRequiredMixin, View):

    def get(self, request, slug, pk):
        self.organization = Organization.objects.get(slug=slug)
        self.event = Event.objects.get(pk=pk)
        self.admin = check_org_admin(get_roles=False, slug=slug, user=request.user)
        if not self.admin:
            messages.add_message(request, messages.ERROR, 'You do not have permissions to perform this action.')
            return redirect(reverse('App_Organization:home'))
        # Prepare event, event staff, then approve
        event_carts = EventCart.objects.filter(event=self.event)
        carts = Cart.objects.filter(cart__in=event_carts)
        event_items = CartItem.objects.filter(cart__in=carts)

        # # Notify Event Venue Admins and Event User that Event Checkout is now available
        event_email_list = []
        event_email_list.append(self.event.user.email)
        if self.event.user_org:
            venue_staff = Staff.objects.filter(organization=self.event.user_org)
            for person in venue_staff.iterator():
                roles = person.roles.all()
                for role in roles:
                    if role.role == 'Admin':
                        if person.user.email not in event_email_list:
                            event_email_list.append(person.user.email)
        print(event_email_list)

        # Add Event Staff
        for item in event_items:
            for x in range(item.quantity):
                new_event_staff = EventStaff.objects.create(
                    event=self.event,
                    product=item.product
                )
                new_event_staff.save()
        # Approve Event
        self.event.approved = True
        self.event.approved_date = timezone.now()
        self.event.save()

        # Create a post announcing the new event

        new_event_post = Post.objects.create(
            organization=self.event.organization,
            event=self.event,
        )
        new_event_post.save()

        # Create second post for event.user_org if exists

        if self.event.user_org:
            new_event_post = Post.objects.create(
                organization=self.event.user_org,
                to_organization=self.event.organization,
                event=self.event,
            )
            new_event_post.save()

        return HttpResponseRedirect(reverse('App_Organization:event_org_review', kwargs={'slug': self.organization.slug, 'pk': self.event.pk}))


@method_decorator(user_is_org_admin, name='dispatch')
class EventStaffRegToggle(LoginRequiredMixin, View):

    def get(self, request, slug, pk):
        organization = get_object_or_404(Organization, slug=slug)
        event = get_object_or_404(Event, pk=pk)

        # Open/close staff registration for the event
        if event.submitted and event.approved:
            event.allow_staff_registration = not event.allow_staff_registration
            event.save()

        roles = Role.objects.filter(Q(role_type__orgtype=event.organization.orgtype, role='Dealer') | Q(role_type__orgtype=event.organization.orgtype, role='Director'))

        # When event staff registration opens, email league dealers/directors to notify of event staff spots open
        if event.allow_staff_registration:
            staff = Staff.objects.filter(organization=event.organization, roles__in=roles)
            staff_email_list = []
            for dealer in staff:
                staff_email_list.append(dealer.user.email)
            print(staff_email_list)
            event_staff_spots = EventStaff.objects.filter(event=event, user=None)

            # set up email context and templates
            current_site = get_current_site(request)
            plaintext_email = get_template('App_Event/email/open_event_staff_registration.txt')
            html_email = get_template('App_Event/email/open_event_staff_registration.html')
            email_context = {
                'sender': 'LeagueBox',
                'spots': event_staff_spots,
                'event': event,
                'event_review_link': 'http://' + str(current_site) + reverse('App_Organization:event_org_review', kwargs={'slug': event.organization.slug, 'pk': event.pk}),
            }
            subject, from_email, to_email = 'LeagueBox says "Event Staff Registration Open!"', settings.NO_REPLY_EMAIL, staff_email_list
            text_content = plaintext_email.render(email_context)
            html_content = html_email.render(email_context)
            msg = EmailMultiAlternatives(subject, text_content, from_email, to_email)
            msg.attach_alternative(html_content, "text/html")
            msg.send()
        return HttpResponseRedirect(reverse('App_Organization:event_org_review', kwargs={'slug': organization.slug, 'pk': event.pk}) + "#staff")


class StaffEventRegAdd(LoginRequiredMixin, View):

    def get(self, request, slug, pk):
        staff_spot = get_object_or_404(EventStaff, pk=pk)
        org_staff = Staff.objects.filter(user=request.user, roles=staff_spot.product.role)
        if org_staff:
            staff_spot.user = request.user
            staff_spot.save()
            messages.add_message(request, messages.SUCCESS, 'You have been added to Event Staff')
        else:
            messages.add_message(request, messages.ERROR, 'You do not have permissions to perform this action.')
        return HttpResponseRedirect(reverse('App_Organization:event_org_review', kwargs={'slug': slug, 'pk': staff_spot.event.pk}))


class EventInit(LoginRequiredMixin, View):

    def get(self, request, pk):
        event = get_object_or_404(Event, pk=pk)

        # validate user is event staff
        event_staff = EventStaff.objects.filter(event=event)
        if request.user.id not in event_staff.values_list('user', flat=True):
            messages.add_message(request, messages.ERROR, 'You do not have permissions to perform this action.')
            return HttpResponseRedirect(reverse('App_Organization:event_org_review', kwargs={'slug': event.organization.slug, 'pk': event.pk}))

        event_carts = EventCart.objects.filter(event=event)
        # check for outstanding event carts still requiring invoicing/payment
        outstanding_carts = 0
        for event_cart in event_carts:
            if not event_cart.cart.purchased:
                outstanding_carts += 1
        if outstanding_carts:
            messages.add_message(request, messages.ERROR, 'This event has outstanding requirements. Please contact organization or event administrator')
            return HttpResponseRedirect(reverse('App_Organization:event_org_review', kwargs={'slug': event.organization.slug, 'pk': event.pk}))

        # Create Initial Subevent
        main = SubEventType.objects.get(name='Main')
        check_subevent = SubEvent.objects.filter(event=event, type=main)
        if check_subevent.exists():
            messages.add_message(request, messages.ERROR, f'This event has already been initialized on {timezone.localtime(event.initialized_date).strftime("%x %X")}')
            return HttpResponseRedirect(reverse('App_Organization:event_org_review', kwargs={'slug': event.organization.slug, 'pk': event.pk}))
        else:
            new_subevent = SubEvent.objects.create(
                event=event,
                type=main,
                status='init'
            )
            new_subevent.save()
            subevent = new_subevent
            messages.add_message(request, messages.SUCCESS, f'Subevent {new_subevent} created.')
            # initialize event after adding main subevent
            event.initialized = True
            event.initialized_date = timezone.localtime(timezone.now())
            event.save()
            messages.add_message(request, messages.SUCCESS, f'Event {event.id} initialized.')
            return HttpResponseRedirect(reverse('App_Event:config', kwargs={'pk': event.id, 'subevent': subevent.id}))


class SubeventConfig(LoginRequiredMixin, TemplateView):
    template_name = 'App_Event/subevent_config_start.html'

    def dispatch(self, request, *args, **kwargs):
        self.subevent = get_object_or_404(SubEvent, id=self.kwargs['subevent'])
        if not self.subevent.status == 'init':
            messages.add_message(self.request, messages.ERROR, f'Event {self.subevent.event.pk}: {self.subevent.type} has already been configured.')
            return HttpResponseRedirect(reverse('App_Organization:event_org_review', kwargs={'slug': self.subevent.event.organization.slug, 'pk': self.subevent.event.pk}))
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        event = get_object_or_404(Event, id=self.kwargs['pk'])
        subevent = self.subevent
        context = {}
        if event.user_org:
            # Get equipment that is at event location and is NOT already assigned to a game
            game_equip_qs = GameEquip.objects.all().values_list('equip', flat=True)
            results = Equipment.objects.exclude(id__in=game_equip_qs).filter(organization=event.organization, location=event.user_org)
            context['equipment'] = results
        else:
            # Get equipment that is stored for private game and is NOT already assigned to a game
            game_equip_qs = GameEquip.objects.all().values_list('equip', flat=True)
            results = Equipment.objects.exclude(id__in=game_equip_qs).filter(organization=event.organization, location=event.organization)
            context['equipment'] = results
        form = SubeventConfigForm(self.request.POST or None)

        context['form'] = form
        context['subevent'] = subevent

        return context


class OpenRegistration(View):
    '''Open registration without adding App_Game model records'''
    # todo: create a create_game function to merge open_reg into one ajax call
    def post(self, request):
        if request.is_ajax():
            json_data = request.POST

            allow_vip = False
            if json_data['allowVip'] == 'true':
                allow_vip = True

            subevent = get_object_or_404(SubEvent, id=json_data['subevent'])
            subevent.status = 'open'
            subevent.allow_coin_boost = allow_vip
            subevent.save()
        return JsonResponse({'status': 200})


def check_open_seats(game):
    '''Returns True if game has open seats'''
    game_equip = GameEquip.objects.filter(game=game)
    seats_qs = GameSeat.objects.filter(game_equip__in=game_equip, seat_user__isnull=True)
    if seats_qs:
        return True


def require_seat_assignment(subevent, user):
    '''Given subevent and user args, this function checks to see if user is registered, but has not yet chosen a seat when seat assignment is not auto'''
    open_seat = False
    has_seat = False
    game_qs = Game.objects.filter(subevent=subevent)
    registered_users_qs = SubEventRegistration.objects.filter(subevent=subevent)
    if game_qs:
        game = game_qs[0]
        auto_seat = game.auto_assign_seats
        registration = registered_users_qs.filter(user=user)
        if len(registration) == 1:
            has_seat = bool(GameSeat.objects.filter(game_equip__game__subevent=subevent, seat_user=registration[0]))
            open_seat = check_open_seats(game)
            if not auto_seat and not has_seat and open_seat:
                print('Game is not auto_seat, has seats available and user has no assigned seat ')
    return open_seat, has_seat


class SubeventReview(LoginRequiredMixin, TemplateView):
    template_name = 'App_Event/subevent_review.html'

    def dispatch(self, request, *args, **kwargs):
        self.subevent = get_object_or_404(SubEvent, pk=self.kwargs['pk'])

        # run function to check to see if user is registered, but has not yet chosen a seat when seat assignment is not auto
        open_seat, has_seat = require_seat_assignment(self.subevent, request.user)
        if open_seat and not has_seat and not self.subevent.game.start:
            return HttpResponseRedirect(reverse('App_Game:choose_seat', kwargs={'pk': self.subevent.game.pk}))
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = {}
        # check for a url tab string variable
        if 'tab' in kwargs:
            context['tab'] = kwargs['tab']

        subevent = self.subevent
        registered_users = SubEventRegistration.objects.filter(subevent=subevent)
        game_seats = GameSeat.objects.filter(game_equip__game__subevent=subevent)

        # get unseated registered users for seating actions after game has started
        non_seated_users = registered_users.filter(gameseat__isnull=True)
        context['unseated'] = non_seated_users.filter(coin_boost=False)
        context['unseated_VIP'] = non_seated_users.filter(coin_boost=True)

        # get game seat for seat card if available
        registration = registered_users.filter(user=self.request.user)
        if len(registration) == 1:
            registration = registration.get(user=self.request.user)
            game_seat = game_seats.filter(seat_user=registration)
            if game_seat:
                context['game_seat'] = game_seat.get(seat_user=registration)

        context['subevent'] = subevent
        context['game_seats'] = game_seats
        context['registered_users'] = registered_users
        for user in registered_users:
            if self.request.user == user.user and not user.closed:
                context['registered'] = True
        # users that have completed the subevent
        ranked_users = GameRank.objects.filter(game=subevent.game).order_by('-created')
        context['ranked_users'] = ranked_users
        context['allow_finalize'] = False
        if ranked_users and not game_seats:
            context['allow_finalize'] = True
        if subevent.game:
            game_equip = GameEquip.objects.filter(game=subevent.game).order_by('label')
            seats_qs = GameSeat.objects.filter(game_equip__in=game_equip, seat_user__isnull=True)
            available_seats_qs = seats_qs.values_list('game_equip', flat=True).distinct()
            available_game_equip = GameEquip.objects.filter(id__in=available_seats_qs).order_by('label')
            context['game_equip'] = available_game_equip

        return context


class RegisterToggle(LoginRequiredMixin, View):
    '''Allow user to toggle users registration on/off for a subevent'''
    def get(self, request, *args, **kwargs):

        subevent = get_object_or_404(SubEvent, pk=kwargs['pk'])
        url = HttpResponseRedirect(reverse('App_Event:sub_review', kwargs={'pk': subevent.pk}))

        # if subevent not open for registration, return user to subevent review
        if not subevent.status == 'open':
            messages.add_message(request, messages.ERROR, f'Event {subevent.event.pk}: {subevent.type} is not accepting changes to registrations. Please notify event administrator.')
            return url

        registrations = SubEventRegistration.objects.filter(subevent=subevent, user=request.user)

        # register user if not registered
        if not registrations:
            registration = SubEventRegistration.objects.create(
                subevent=subevent,
                user=request.user
            )
            registration.save()
            messages.add_message(request, messages.SUCCESS, f'You have successfully registered for Event {subevent.event.pk}: {subevent.type}.')

            # Redirect to allow user to choose a seat if the game does not auto assign
            if Game.objects.filter(subevent=subevent):
                if not subevent.game.auto_assign_seats:
                    return HttpResponseRedirect(reverse('App_Game:choose_seat', kwargs={'pk': subevent.game.pk}))

        # unregister if registration hasn't been processed and marked as closed
        else:
            registration = SubEventRegistration.objects.get(subevent=subevent, user=request.user)
            if registration.closed:
                messages.add_message(
                    request,
                    messages.ERROR,
                    f'This registration has been processed and closed on {registration.closed_date.strftime("%d%b%Y %H:%M")}. Unregister is not allowed.'
                )
            else:
                seats = GameSeat.objects.filter(game_equip__game__subevent=subevent, seat_user=registration)
                if len(seats) == 1:
                    seat = seats[0]
                    seat.seat_user = None
                    seat.save()
                    messages.add_message(
                        request,
                        messages.SUCCESS,
                        f'You have been successfully removed from {seat.game_equip.equip.product.name}: #{seat.game_equip.label} Seat: #{seat.seat}'
                    )
                registration.delete()
                messages.add_message(request, messages.SUCCESS, f'You have successfully unregistered from Event {subevent.event.pk}: {subevent.type}.')

        return url


class AddSubevent(View):
    def post(self, request):
        json_data = request.POST

        if json_data['eventId']:
            event = Event.objects.get(pk=json_data['eventId'])
            new_subevent = SubEvent.objects.create(
                event=event,
                type=SubEventType.objects.get(name='Side'),
                status='init'
            )
            new_subevent.save()

            success_URL = reverse('App_Event:config', kwargs={'pk': event.pk, 'subevent': new_subevent.pk})
            print(success_URL)

            response_data = {}
            response_data['status'] = 200
            response_data['successUrl'] = success_URL
            return JsonResponse(response_data, status=200)
        else:
            return JsonResponse({'status': 'false'}, status=500)


class AllowSubeventRegistrationToggle(View):
    '''Event staff can open/close allowing registration for this subevent'''

    def get(self, request, pk):
        subevent = get_object_or_404(SubEvent, pk=pk)
        print(subevent.status)
        if subevent.status == 'open':
            subevent.status = 'closed'
            subevent.save()
        elif subevent.status == 'closed':
            subevent.status = 'open'
            subevent.save()
        return HttpResponseRedirect(reverse('App_Event:sub_review_tab', kwargs={'pk': subevent.pk, 'tab': 'actions'}))


class FinalizeSubeventStart(IsEventStaffMixin, LoginRequiredMixin, TemplateView):
    template_name = 'App_Event/subevent_finalize.html'

    def dispatch(self, request, *args, **kwargs):
        print(self.request.path)
        pk = kwargs['pk']
        self.subevent = get_object_or_404(SubEvent, pk=pk)
        event_staff = EventStaff.objects.filter(event=self.subevent.event)
        if self.request.user.id not in event_staff.values_list('user', flat=True):
            messages.add_message(request, messages.ERROR, 'You do not have permissions to perform this action')
            return HttpResponseRedirect(reverse('App_Event:sub_review', kwargs={'pk': pk}))

        # verify no more equipment is assigned to subevent if subevent is a game
        if self.subevent.game:
            equip = GameEquip.objects.filter(game=self.subevent.game)
            if equip:
                messages.add_message(request, messages.ERROR, 'Finalize failed. Subevent/Game still has assigned sections/equipment.')
                return HttpResponseRedirect(reverse('App_Event:sub_review', kwargs={'pk': pk}))

        # verify all registrations have been closed
        registrations = SubEventRegistration.objects.filter(subevent=self.subevent, closed=False)
        if registrations:
            messages.add_message(request, messages.ERROR, 'Finalize failed. Subevent/Game still has unresolved registrations.')
            return HttpResponseRedirect(reverse('App_Event:sub_review', kwargs={'pk': pk}))

        # verify subevent status is closed
        if self.subevent.status != 'closed':
            messages.add_message(request, messages.ERROR, 'Finalize failed. Subevent/Game registration status is not closed.')
            return HttpResponseRedirect(reverse('App_Event:sub_review', kwargs={'pk': pk}))

        # if event doesn't require game finalize dialog, finalize the subevent and return to the event review
        if not self.subevent.game:
            self.subevent.finalized = True
            self.subevent.finalized_date = timezone.now()
            self.subevent.save()
            messages.add_message(request, messages.SUCCESS, 'Subevent finalize completed.')
            return HttpResponseRedirect(reverse('App_Organization:event_org_review', kwargs={'slug': self.subevent.event.organization.slug, 'pk': self.subevent.event.pk}))

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['subevent'] = self.subevent
        context['rank_sets'] = RankSet.objects.filter(organization=self.subevent.event.organization)
        if self.subevent.game:
            context['base_points'] = self.subevent.game.base_points
            context['ranked_users'] = self.subevent.game.ranked_users
        return context


class GetRankStructure(View):

    def post(self, request):
        json_data = request.POST
        print(json_data)
        subevent = SubEvent.objects.filter(id=json_data['subeventId'])
        if subevent:
            subevent = subevent[0]
            closed_registrations = SubEventRegistration.objects.filter(subevent=subevent, closed=True)
            rank_set = RankSet.objects.get(id=json_data['rankSet'])
            rank_ranges = RankRange.objects.filter(rank_set=rank_set)

            # match the number of closed registrations to a range in the set
            finalize_range = None
            for range in rank_ranges:
                if re.match(range.regex, str(closed_registrations.count())):
                    finalize_range = range
            print(finalize_range)
            ranks = {}
            rank_structure = RankStructure.objects.filter(rank_range=finalize_range)
            rank_num = 0
            for rank in rank_structure:
                ranks[rank_num] = {
                    'place': rank.place,
                    'percentage': rank.percentage,
                    'flat_points': rank.flat_points
                }
                rank_num += 1
            response_data = {'ranks': ranks}
            response_data['range'] = {'rangeId': finalize_range.id, 'start': finalize_range.start, 'end': finalize_range.end}
            # pass url to process rankings
            finalize_url = reverse('App_Event:process', kwargs={'pk': subevent.pk, 'rank_set': rank_set.id})
            response_data['finalizeUrl'] = finalize_url
            return JsonResponse(response_data, status=200)
        else:
            return JsonResponse({'status': 'false'}, status=500)


class ProcessRanking(IsEventStaffMixin, LoginRequiredMixin, View):

    def get(self, request, pk, rank_set):
        subevent = get_object_or_404(SubEvent, pk=pk)
        process_set = get_object_or_404(RankSet, pk=rank_set)
        if subevent.game:
            game_ranks = GameRank.objects.filter(game=subevent.game.pk).order_by('-created')
            rank_ranges = RankRange.objects.filter(rank_set=process_set)
            # match the number of closed registrations to a range in the set
            finalize_range = None
            for range in rank_ranges:
                if re.match(range.regex, str(rank_ranges.count())):
                    finalize_range = range

            structure = RankStructure.objects.filter(rank_range=finalize_range)

            rank_num = 1
            for rank in game_ranks:
                if rank.registration.closed is False:
                    messages.add_message(request, messages.ERROR, 'Finalize failed. Subevent/Game still has unresolved registrations.')
                    return HttpResponseRedirect(reverse('App_Event:sub_review', kwargs={'pk': subevent.pk}))
                rank.rank = rank_num
                # if rank place equals the place in the structure, calculate points and save to the model
                for place in structure:
                    if rank.rank == place.place:
                        if place.place:
                            rank.points = (place.percentage/100) * subevent.game.base_points
                        if place.flat_points:
                            rank.points = place.flat_points

                    if (rank.points is None) and (place.place is None):
                        if place.percentage:
                            rank.points = (place.percentage/100) * subevent.game.base_points
                        if place.flat_points:
                            rank.points = place.flat_points

                rank.save()
                print(rank.rank)
                rank_num += 1

        subevent.finalized = True
        subevent.finalized_date = timezone.now()
        subevent.save()
        subevent.game.end = timezone.now()
        subevent.game.save()
        messages.add_message(request, messages.SUCCESS, 'Subevent finalized.')
        return HttpResponseRedirect(reverse('App_Organization:event_org_review', kwargs={'slug': subevent.event.organization.slug, 'pk': subevent.event.pk}))
