from App_Event.models import EventStaff, SubEventRegistration
from django import template

from App_Shop.models import CartItem, Cart, EventCart

register = template.Library()


@register.filter(name='get_qty')
def get_qty(product, event):
    event_carts = EventCart.objects.filter(event=event)
    if event_carts:
        event_cart = EventCart.objects.get(event=event)
        cart = Cart.objects.get(cart=event_cart)
        cart_items = CartItem.objects.filter(cart=cart, product=product)
        if cart_items:
            cart_item = CartItem.objects.get(cart=cart, product=product)
            return cart_item.quantity
    else:
        return 0


@register.filter(name='get_item_total')
def item_total(product, event):
    event_carts = EventCart.objects.filter(event=event)
    if event_carts:
        event_cart = EventCart.objects.get(event=event)
        cart = Cart.objects.get(cart=event_cart)
        cart_items = CartItem.objects.filter(cart=cart, product=product)
        if cart_items:
            cart_item = CartItem.objects.get(cart=cart, product=product)
            return format(float(cart_item.quantity) * float(cart_item.product.price), '.2f')
    else:
        return 0


@register.filter(name='cart_total')
def cart_total(event_cart):
    cart_items = CartItem.objects.filter(cart=event_cart.cart)
    total = 0.00
    if cart_items:
        for item in cart_items:
            total += float(item.quantity) * float(item.product.price)
        return format(total, '.2f')
    else:
        return total


@register.filter(name='get_invoice_url')
def get_invoice_url(invoice, cart_id):
    event_carts = EventCart.objects.filter(cart=cart_id)
    if event_carts:
        event_cart = EventCart.objects.get(cart=cart_id)
        if invoice['event_cart_id'] == event_cart.id:
            return invoice['url']


@register.filter(name='get_invoice_status')
def get_invoice_status(invoice, cart_id):
    event_carts = EventCart.objects.filter(cart=cart_id)
    if event_carts:
        event_cart = EventCart.objects.get(cart=cart_id)
        if invoice['event_cart_id'] == event_cart.id:
            return invoice['status'].upper()


@register.filter(name='is_event_staff')
def is_event_staff(user, event):
    event_staff = EventStaff.objects.filter(event=event, user=user)
    if event_staff:
        return True


@register.filter_function
def order_by(queryset, args):
    args = [x.strip() for x in args.split(',')]
    return queryset.order_by(*args)


@register.filter_function
def seats_empty(queryset, null_bool):
    return queryset.filter(seat_user__isnull=null_bool).count()


@register.filter_function
def is_subevent_registered(subevent, user):
    registered_user = SubEventRegistration.objects.filter(subevent=subevent, user=user)
    if registered_user:
        return True
    else:
        return False


@register.filter_function
def is_registered_any(user):
    '''Checks to see if user has any open registrations'''
    registered_user = SubEventRegistration.objects.filter(user=user, closed=False)
    if registered_user:
        return True


@register.filter_function
def check_registration_status(subevent, status):
    if subevent.status == status:
        return True
