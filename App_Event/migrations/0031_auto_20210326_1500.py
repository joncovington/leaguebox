# Generated by Django 3.1.5 on 2021-03-26 21:00

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('App_Event', '0030_auto_20210321_1527'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='subeventregistration',
            unique_together={('subevent', 'user')},
        ),
    ]
