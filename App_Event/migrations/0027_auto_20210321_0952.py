# Generated by Django 3.1.5 on 2021-03-21 15:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('App_Event', '0026_subeventregistration_vip'),
    ]

    operations = [
        migrations.RenameField(
            model_name='subeventregistration',
            old_name='vip',
            new_name='coin_boost',
        ),
    ]
