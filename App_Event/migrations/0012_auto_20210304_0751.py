# Generated by Django 3.1.5 on 2021-03-04 14:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('App_Event', '0011_eventstaff'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='eventstaff',
            options={'verbose_name_plural': 'Event Staff'},
        ),
    ]
