from functools import wraps
from django.shortcuts import HttpResponseRedirect, get_object_or_404
from django.contrib import messages
from django.urls.base import reverse

from App_Event.models import Event


def check_user_event(user, event_pk):
    event = get_object_or_404(Event, id=event_pk)
    if event.user == user:
        return True
    else:
        return False


# decorator to check user is event creator
def is_event_user(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        event_admin = check_user_event(user=request.user, event_pk=kwargs['pk'])
        if event_admin:
            return function(request, *args, **kwargs)
        else:
            messages.error(request, 'You do not have permissions to access the previous page.')
            return HttpResponseRedirect(reverse('App_Organization:home'))
    return wrap
