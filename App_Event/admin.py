from django.contrib import admin
from App_Event.models import Event, EventDetail, EventStaff, SubEventType, SubEvent, SubEventRegistration
# Register your models here.


@admin.register(SubEvent)
class SubEventAdmin(admin.ModelAdmin):
    readonly_fields = ('created_date', )
    list_display = ('type', 'event_date', 'provider', 'host')

    def event_date(self, obj):
        return obj.event.start.strftime('%Y-%m-%d')

    def provider(self, obj):
        return obj.event.organization

    def host(self, obj):
        if obj.event.user_org:
            return obj.event.user_org
        else:
            return None


admin.site.register(Event)
admin.site.register(EventDetail)
admin.site.register(EventStaff)
admin.site.register(SubEventType)
admin.site.register(SubEventRegistration)
