from datetime import timedelta
from django.utils import timezone
from django.db import models
from django.conf import settings


from phone_field import PhoneField
from localflavor.us.models import USZipCodeField, USStateField

from App_Organization.models import Organization


class Event(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING, related_name='event_user')
    user_org = models.ForeignKey(Organization, on_delete=models.DO_NOTHING, related_name='event_user_org', blank=True, null=True, help_text='Pick the organization hosting the event.')
    organization = models.ForeignKey(Organization, on_delete=models.DO_NOTHING, related_name='event_org')
    created = models.DateTimeField(auto_now_add=True)
    start = models.DateTimeField(help_text='Pick your event date and time. Required.')
    end = models.DateTimeField(blank=True, null=True, help_text='Specific end date/time for your event. Not required.')
    submitted = models.BooleanField(default=False)
    submitted_date = models.DateTimeField(blank=True, null=True)
    approved = models.BooleanField(default=False)
    approved_date = models.DateTimeField(blank=True, null=True)
    initialized = models.BooleanField(default=False)
    initialized_date = models.DateTimeField(blank=True, null=True)
    fulfilled = models.BooleanField(default=False)
    fulfilled_date = models.DateTimeField(blank=True, null=True)
    allow_staff_registration = models.BooleanField(default=False)

    def __str__(self):
        return f'{str(self.start)} | {self.organization.organization_name} | {self.user.email}'

    @property
    def is_expired(self):
        if self.initialized or self.fulfilled:
            return False
        expires_date = timezone.localtime(self.start).date() + timedelta(days=1)
        return timezone.localtime(timezone.now()).date() >= expires_date


class EventDetail(models.Model):
    event = models.OneToOneField(Event, on_delete=models.CASCADE, related_name='detail')
    address_1 = models.CharField(max_length=255)
    address_2 = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=30)
    state = USStateField()
    postal_code = USZipCodeField()
    phone = PhoneField(help_text='Event Contact Phone Number')
    requests = models.CharField(max_length=512, blank=True, null=True)

    def __str__(self):
        return f'{str(self.event.start)} | {self.event.organization.organization_name} | {self.event.user.email}'


class EventStaff(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='event_staff_event')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True, related_name='event_staff_user')
    product = models.ForeignKey('App_Shop.Product', on_delete=models.CASCADE, related_name='event_staff_product')

    class Meta:
        verbose_name_plural = "Event Staff"

    def __str__(self):
        staff_name = 'Not Assigned'
        event_datetime = self.event.start.strftime("%d%b%Y %H:%M")
        if self.user:
            staff_name = self.user.profile.get_profile_firstlast()
        return f'{event_datetime} | {self.event.user_org} | {self.product.name} | {staff_name}'

    def get_role(self):
        return self.product.role


class SubEventType(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name = "Subevent Type"
        verbose_name_plural = "Subevent Types"

    def __str__(self):
        return self.name


STATUS_CHOICES = (('init', 'Init'), ('open', 'Open'), ('closed', 'Closed'))


class SubEvent(models.Model):
    event = models.ForeignKey(Event, on_delete=models.DO_NOTHING, related_name='subevents')
    type = models.ForeignKey(SubEventType, on_delete=models.DO_NOTHING, related_name='subevent_type')
    created_date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(
        max_length=20,
        choices=STATUS_CHOICES,
        null=True,
        blank=True,
    )
    allow_coin_boost = models.BooleanField(default=False)
    finalized = models.BooleanField(default=False)
    finalized_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return f'{self.type} {self.event.id}'

    @property
    def allow_reg(self):
        if self.status == 'open':
            return True

    @property
    def needs_config(self):
        if self.status == 'init':
            return True


class SubEventRegistration(models.Model):
    subevent = models.ForeignKey(SubEvent, on_delete=models.DO_NOTHING, related_name='subevent_reg')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING, related_name='subevent_user')
    coin_boost = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    closed = models.BooleanField(default=False)
    closed_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        verbose_name = "Subevent Registration"
        verbose_name_plural = "Subevent Registrations"
        unique_together = ('subevent', 'user')
        ordering = ['subevent', 'created_date']

    def __str__(self):
        return f'{self.subevent.type} | {self.subevent.event.id} | {self.user.profile.display_name}'
