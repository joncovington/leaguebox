import datetime
from django import forms
from django.db.models import Q
from bootstrap_datepicker_plus import DateTimePickerInput

from App_Event.models import Event, EventDetail

from App_Organization.models import Organization

from App_Game.models import GameType

from App_Equipment.models import Equipment


class NewEventForm(forms.ModelForm):

    recurring = forms.BooleanField(label='Weekly Recurring Event', required=False, help_text='Select if you would like to have a weekly recurring event.')

    class Meta:
        model = Event
        fields = ['user_org', 'start', 'end', ]
        widgets = {
            'start': DateTimePickerInput(
                options={
                    'minDate': (datetime.datetime.today() + datetime.timedelta(days=1)).strftime('%Y-%m-%d 00:00:00'),
                    'enabledHours': [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, ],
                },
            ),
            'end': DateTimePickerInput(
                options={
                    'minDate': (datetime.datetime.today() + datetime.timedelta(days=1)).strftime('%Y-%m-%d 00:00:00'),
                }
            )
        }

    def __init__(self, *args, **kwargs):
        self.organization = kwargs.pop('organization', None)
        self.user = kwargs.pop('user', None)
        super(NewEventForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['user_org'].queryset = Organization.objects.filter(~Q(staff_organization__organization=self.organization), staff_organization__user=self.user, orgtype__orgtype='Venue')
            self.fields['user_org'].label = 'Hosting Organization'
            self.fields['user_org'].empty_label = 'User Hosted'
            self.fields['start'].label = 'Start Date'
            self.fields['end'].label = 'End Date'


class NewEventDetailForm(forms.ModelForm):
    requests = forms.CharField(
        required=False,
        widget=forms.Textarea()
    )

    class Meta:
        model = EventDetail
        exclude = ['event', ]

    def __init__(self, *args, **kwargs):
        self.organization = kwargs.pop('user_org', None)
        self.event = kwargs.pop('event', None)
        super(NewEventDetailForm, self).__init__(*args, **kwargs)
        if self.instance:
            self.fields['requests'].label = 'Additional Requests or Information'
            if EventDetail.objects.filter(event=self.event).exists():
                self.event_detail = EventDetail.objects.get(event=self.event)
                self.fields['address_1'].initial = self.event_detail.address_1
                self.fields['address_2'].initial = self.event_detail.address_2
                self.fields['city'].initial = self.event_detail.city
                self.fields['state'].initial = self.event_detail.state
                self.fields['postal_code'].initial = self.event_detail.postal_code
                self.fields['phone'].initial = self.event_detail.phone
                self.fields['requests'].initial = self.event_detail.requests
            else:
                if self.organization:
                    self.fields['address_1'].initial = self.organization.address_1
                    self.fields['address_2'].initial = self.organization.address_2
                    self.fields['city'].initial = self.organization.city
                    self.fields['state'].initial = self.organization.state
                    self.fields['postal_code'].initial = self.organization.postal_code
                    self.fields['phone'].initial = self.organization.phone


class SubeventConfigForm(forms.Form):

    create_game = forms.BooleanField(
        label="Create Game",
        help_text='Select if this subevent will require a game',
        widget=forms.CheckboxInput(
            attrs={'class': 'MyClass'}
        ),
        required=False,
    )

    game_type = forms.ModelChoiceField(
        queryset=GameType.objects.all(),
        required=False,
        label='Game Type:'
    )

    game_equipment = forms.ModelMultipleChoiceField(
        queryset=Equipment.objects.all(),
        required=False
    )
