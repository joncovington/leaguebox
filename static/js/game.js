
function getSeats(equipSelect, seatSelect, submitBtn, ajaxUrl) {
    submitBtn.attr("disabled", true)
    ajaxUrl = ajaxUrl
    data = {
        equipId: equipSelect
    }
    $.ajax({
        url: ajaxUrl,
        type: "POST",
        headers: {'X-CSRFToken': csrftoken},
        data: data,
        success: function(json) {
            var index, len;
            var seatList = json['seat_list'];
            // remove all items from the seatSelect list, then repopulate with the seats from returned object
            while (seatSelect.firstChild) {
                seatSelect.removeChild(seatSelect.lastChild);
            }
            for (index = 0; index < seatList.length; ++index) {
                var option = document.createElement("option");
                option.text = 'Seat ' + seatList[index];
                option.value = seatList[index];
                seatSelect.add(option);
            }

            submitBtn.attr("disabled", false)
        },
        error : function(xhr,errmsg,err) {
            console.log(xhr.status + ": " + xhr.responseText);
        },
    });
}

function moveSeat(userSeatId, equipSelect, seatId, submitBtn, ajaxUrl) {
    submitBtn.attr("disabled", true)
    ajaxUrl = ajaxUrl
    data = {
        equipId: equipSelect,
        seatId: seatId,
        userSeatId: userSeatId
    }
    $.ajax({
        url: ajaxUrl,
        type: "POST",
        headers: {'X-CSRFToken': csrftoken},
        data: data,
        success: function(json) {
            console.log(json)
            location.reload()
        },
        error : function(xhr,errmsg,err) {
            console.log(xhr.status + ": " + xhr.responseText);
        },
    });
}

function assignSeat(equipId, seatId, gameId, regId, submitBtn, ajaxUrl, successURL) {
    submitBtn.attr("disabled", true)
    submitBtn.append(" <i class='fa fa-spinner fa-spin'></i>").attr("disabled", "");
    data = {
        equipId: equipId,
        seatId: seatId,
        gameId: gameId,
        regId: regId,
    };
    $.ajax({
        url: ajaxUrl,
        type: "POST",
        headers: {'X-CSRFToken': csrftoken},
        data: data,
        success: function(json) {
            $('#notification-success').text('Seat assignment successful!');
            $('#notification-success').removeClass('d-none');
            nextPage(1500, successURL);
        },
        error : function(xhr,errmsg,err) {
            console.log(xhr.status + ": " + xhr.responseText);
        },
    });
}