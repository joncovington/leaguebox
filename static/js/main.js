function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function nextPage(ms, URL) {
    await sleep(ms)
    window.location.href = URL
}
