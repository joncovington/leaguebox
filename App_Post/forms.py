from django import forms

from App_Post.models import Comment, Post, PostImage


class NewPostForm(forms.ModelForm):
    title = forms.CharField(
        required=False,
        label='',
        widget=forms.TextInput(attrs={'placeholder': 'Enter Post Title (Optional)'})
        )
    content = forms.CharField(
        required=False,
        label='',
        widget=forms.Textarea(attrs={'placeholder': 'Enter Post Content'})
        )
    commenting = forms.BooleanField(
        required=False,
        label='Allow Commenting',
        widget=forms.CheckboxInput()
        )

    class Meta:
        model = Post
        fields = [
            'title',
            'content',
            'commenting'
        ]


class NewPostImageForm(forms.ModelForm):
    # image = forms.ImageField(
    #     widget=forms.FileInput(attrs={'class': 'hide-me'})
    # )
    caption = forms.CharField(
        required=False,
        label='',
        widget=forms.TextInput(attrs={'placeholder': 'Enter Caption (Optional)'})
    )

    class Meta:
        model = PostImage
        fields = [
            # 'image',
            'caption'
        ]


class CommentForm(forms.ModelForm):
    comment = forms.CharField(
        label='',
        widget=forms.TextInput(attrs={'placeholder': 'Enter Your Comment'})
    )

    class Meta:
        model = Comment
        fields = [
            'comment'
        ]
