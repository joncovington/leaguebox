# Generated by Django 3.1.5 on 2021-02-17 22:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('App_Post', '0003_comment_hidden'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postimage',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='post_images/'),
        ),
    ]
