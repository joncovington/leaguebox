from django.urls import path
from App_Post import views

app_name = 'App_Post'

urlpatterns = [
    path('like/<str:page>/<pk>/', views.liked, name='like'),
    path('unlike/<str:page>/<pk>/', views.unliked, name='unlike'),
    path('<pk>/', views.post_view, name='post'),
    path('toggle_hide/<str:page>/<postpk>/<pk>/', views.toggle_hide, name='toggle_hide'),
]
