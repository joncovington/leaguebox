from App_Event.models import Event
from django.db import models

from App_Organization.models import Organization
from App_Login.models import User

# Create your models here.


class Post(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, related_name='post_org')
    at_organization = models.ForeignKey(Organization, on_delete=models.CASCADE, related_name='at_org', null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    content = models.CharField(max_length=511, blank=True, null=True)
    commenting = models.BooleanField(default=False, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='post_event', null=True, blank=True)

    class Meta:
        ordering = ['-created', ]

    def __str__(self):
        return f'{self.id} | {self.created} | {self.organization.organization_name}'

    def save(self, *args, **kwargs):
        return super().save(*args, **kwargs)


class PostImage(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='postimages')
    image = models.ImageField(upload_to='post_images/', blank=True, null=True)
    caption = models.CharField(max_length=255, blank=True, null=True)


class Like(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='liked_post')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='liked_user')
    date_created = models.DateTimeField(auto_now_add=True)


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='post_comment')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comment_user')
    comment = models.TextField(verbose_name='Enter your comment')
    comment_date = models.DateTimeField(auto_now_add=True)
    hidden = models.BooleanField(default=False)

    class Meta:
        ordering = ['-comment_date']

    def __str__(self):
        date = self.comment_date.strftime("%A, %d. %B %Y %I:%M%p")
        return f'{date} {self.post.id} | {self.comment}'
