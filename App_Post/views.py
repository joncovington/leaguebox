from django.contrib import messages
from django.shortcuts import render, HttpResponseRedirect, reverse, get_object_or_404
from django.contrib.auth.decorators import login_required

from App_Post.models import Comment, Post, Like
from App_Post.forms import CommentForm

from App_Organization.views import check_roles


def build_redirect(page, post):
    if page == 'org':
        redirect_str = reverse('App_Organization:organization', kwargs={'slug': post.organization.slug}) + '#post_' + str(post.pk)
        return redirect_str
    elif page == 'home':
        redirect_str = reverse('App_Organization:home' + '#post_' + str(post.pk))
        return redirect_str
    elif page == 'post':
        redirect_str = reverse('App_Post:post', kwargs={'pk': post.pk})
        return redirect_str
    else:
        redirect_str = reverse('App_Organization:home')


# functions to like/unlike an organization post per user
@login_required
def liked(request, page, pk):
    post = Post.objects.get(pk=pk)
    already_liked = Like.objects.filter(post=post, user=request.user)
    if not already_liked:
        liked_post = Like(post=post, user=request.user)
        liked_post.save()
    return HttpResponseRedirect(build_redirect(page, post))


@login_required
def unliked(request, page, pk):
    post = Post.objects.get(pk=pk)
    already_liked = Like.objects.get(post=post, user=request.user)
    already_liked.delete()
    return HttpResponseRedirect(build_redirect(page, post))


def post_view(request, pk):
    context = {}
    post = get_object_or_404(Post, pk=pk)
    context['post'] = post
    context['page_title'] = '{}: Post #{}'.format(post.organization.organization_name, post.id)
    admin = False
    if request.user.is_authenticated:
        liked_post_list = Like.objects.filter(user=request.user).values_list('post', flat=True)
        context['liked_post_list'] = liked_post_list
        user_roles, admin = check_roles(post.organization, request.user)

    if post.commenting:
        form = CommentForm()
        context['form'] = form
        if request.method == 'POST':
            form = CommentForm(request.POST)
            context['form'] = form
            if form.is_valid():
                comment = Comment()
                comment.post = post
                comment.user = request.user
                comment.comment = form.cleaned_data['comment']
                comment.save()
                return HttpResponseRedirect(build_redirect('post', post))

    if not admin:
        comments = Comment.objects.filter(post=post, hidden=False)
    else:
        comments = Comment.objects.filter(post=post)
        context['admin'] = admin
        hidden_comments_list = comments.filter(hidden=True).values_list('pk', flat=True)
        context['hidden_comments_list'] = hidden_comments_list

    context['comments'] = comments

    return render(request, 'App_Post/view_post.html', context=context)


@login_required
def toggle_hide(request, page, postpk, pk):
    post = Post.objects.get(pk=postpk)
    user_roles, admin = check_roles(post.organization, request.user)
    if admin:
        comment = Comment.objects.get(pk=pk)
        comment.hidden = not comment.hidden
        comment.save()
    else:
        messages.error(request, 'You do not have permissions to perform this action.')
    return HttpResponseRedirect(build_redirect(page, post))
