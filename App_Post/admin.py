from django.contrib import admin
from App_Post.models import Post, Like, Comment, PostImage


admin.site.register(Post)
admin.site.register(PostImage)
admin.site.register(Like)
admin.site.register(Comment)
