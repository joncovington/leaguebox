from django.test import TestCase, Client
from django.urls import reverse

from App_Login.models import User
from App_Organization.models import Application, OrgType, Role, Staff


class TestViews(TestCase):

    def setUp(self):
        self.user = User.objects.create(email='testviews@leaguebox.fun')
        self.user.profile.display_name = '@testuser'
        self.user.profile.first_name = 'Test'
        self.user.profile.last_name = 'User'
        self.user.profile.save()
        self.user.set_password('test12345')
        self.user.save()

        self.app_org_type = OrgType.objects.create(orgtype='League')
        self.role = Role.objects.create(
            role_type=self.app_org_type,
            role='Admin'
        )

        self.application = Application.objects.create(
            user=self.user,
            organization_name='Test Organization',
            orgtype=self.app_org_type,
            first_name='Test',
            last_name='Test',
            address_1='Test Address',
            city='Test City',
            state='UT',
            postal_code='84116',
            phone='8010000000',
            service_description='Description of offered services'
        )
        self.application.save()

        self.client = Client()
        self.logged_in = self.client.login(username=self.user.email, password='test12345')
        self.home_url = reverse('App_Organization:home')
        self.application_custom_url = reverse('App_Organization:application', kwargs={'org_type': self.app_org_type.orgtype})

    def test_home_GET(self):
        response = self.client.get(self.home_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'App_Organization/home.html')

    def test_application_custom_GET(self):
        response = self.client.get(self.application_custom_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'App_Organization/application.html')

    def test_application_custom_POST(self):
        response = self.client.post(self.application_custom_url, {
            'organization_name': 'Test League',
            'first_name': 'Test',
            'last_name': 'User',
            'address_1': 'Test Address',
            'address_2': '',
            'city': 'Test City',
            'state': 'UT',
            'postal_code': '00000',
            'phone_0': '0000000000',
            'phone_1': '',
            'service_description': 'Test Description'
        })
        self.assertEquals(Application.objects.filter(organization_name='Test League').count(), 1)
        self.assertRedirects(
            response,
            reverse('App_Organization:application_status'),
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True
        )

    def test_application_status_GET(self):
        response = self.client.get(reverse('App_Organization:application_status'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'App_Organization/application_status.html')

    def test_application_approve(self):
        self.user.is_staff = True
        self.user.save()

        response = self.client.get(reverse('App_Organization:approve', kwargs={'pk': self.application.pk}))
        self.assertRedirects(
            response,
            reverse('App_Organization:application_status'),
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True
        )
        application_after = Application.objects.get(pk=self.application.pk)
        self.assertEquals(application_after.approved, True)
        staff_qs_count = Staff.objects.all().count()
        self.assertEquals(staff_qs_count, 1)
        self.user.is_staff = False
        self.user.save()

    def test_application_archive(self):
        self.user.is_staff = True
        self.user.save()
        response = self.client.get(reverse('App_Organization:archive', kwargs={'pk': self.application.pk}))
        self.assertRedirects(
            response,
            reverse('App_Organization:application_status'),
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True
        )
        application_after = Application.objects.get(pk=self.application.pk)
        self.assertEquals(application_after.archived, True)

    def test_application_reject(self):
        self.user.is_staff = True
        self.user.save()
        response = self.client.get(reverse('App_Organization:reject', kwargs={'pk': self.application.pk}))
        self.assertRedirects(
            response,
            reverse('App_Organization:application_status'),
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=True
        )
        application_after = Application.objects.get(pk=self.application.pk)
        self.assertEquals(application_after.approved, False)
        self.assertEquals(application_after.rejected, True)
