from django.test.testcases import TestCase
from django.urls import reverse, resolve

from App_Login.models import User
from App_Organization.models import Application, OrgType

from App_Organization.views import home, application_custom, application_status, ApplicationDetailView


class TestUrls(TestCase):

    def setUp(self):
        # create a test user
        self.user = User.objects.create(email='testemail@leaguebox.fun')
        self.app_org_type = OrgType.objects.create(orgtype='League')
        self.application = Application.objects.create(
            user=self.user,
            organization_name='Test Organization',
            orgtype=self.app_org_type,
            first_name='Test',
            last_name='Test',
            address_1='Test Address',
            city='Test City',
            state='UT',
            postal_code='84116',
            phone='8010000000',
            service_description='Description of offered services'
        )

    def test_home_url_resolves(self):
        url = reverse('App_Organization:home')
        self.assertEquals(resolve(url).func, home)

    def test_application_url_resolves(self):
        url = reverse('App_Organization:application', kwargs={'org_type': self.app_org_type.pk})
        self.assertEquals(resolve(url).func, application_custom)

    def test_application_status_url_resolves(self):
        url = reverse('App_Organization:application_status')
        self.assertEquals(resolve(url).func, application_status)

    def test_application_detail_url_ressolves(self):
        url = reverse('App_Organization:application_detail', kwargs={'pk': self.application.pk})
        self.assertEquals(resolve(url).func.view_class, ApplicationDetailView)
