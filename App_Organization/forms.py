from django import forms
from App_Organization.models import Application, Organization, OrgType
from localflavor.us.forms import USZipCodeField


class ApplicationAdminForm(forms.ModelForm):
    class Meta:
        model = Application
        fields = '__all__'


class OrganizationAdminForm(forms.ModelForm):
    class Meta:
        model = Organization
        fields = '__all__'


class ApplicationForm(forms.ModelForm):
    organization_name = forms.CharField(label='Organization Name')
    postal_code = USZipCodeField(widget=forms.TextInput(attrs={'placeholder': 'xxxxx or xxxxx-xxxx'}))

    class Meta:
        model = Application
        exclude = ['user', 'orgtype', 'application_date', 'approved', 'approved_date', 'archived']


class OrganizationEdit(forms.ModelForm):
    first_name = forms.CharField(label='Primary Contact First Name')
    last_name = forms.CharField(label='Primary Contact Last Name')
    postal_code = USZipCodeField(widget=forms.TextInput(attrs={'placeholder': 'xxxxx or xxxxx-xxxx'}))

    class Meta:
        model = Organization
        exclude = ['orgtype', 'slug', 'organization_name', 'created', 'update_date']
