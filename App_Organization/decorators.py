from functools import wraps
from django.shortcuts import HttpResponseRedirect, get_object_or_404
from django.contrib import messages
from django.urls.base import reverse
from App_Organization.models import Organization, Staff


def check_org_admin(get_roles=bool, slug=str, user=object):

    organization = get_object_or_404(Organization, slug=slug)
    staff = Staff.objects.filter(organization=organization, user=user)
    org_type = organization.orgtype.orgtype
    admin_bool = False
    user_roles = []
    if len(staff) > 1:
        messages.error('Error checking for Staff Role. Please notify Administrator')
        admin_bool = False
        return admin_bool
    if len(staff) > 0:
        for person in staff.iterator():
            roles = person.roles.all()
            for role in roles:
                role_pair = (role.role_type.orgtype, role.role)
                user_roles.append(role_pair)
        for role in user_roles:
            if role == (org_type, 'Admin'):
                admin_bool = True
    else:
        admin_bool = False
    if get_roles:
        return user_roles, admin_bool
    return admin_bool


# decorator to check user admin within an organization based on slug
def user_is_org_admin(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        admin = check_org_admin(get_roles=True, slug=kwargs['slug'], user=request.user)
        if admin:
            return function(request, *args, **kwargs)
        else:
            messages.error(request, 'You do not have permissions to access the previous page.')
            return HttpResponseRedirect(reverse('App_Organization:home'))
    return wrap
