from django.contrib import admin
from App_Organization.models import Application, League, Venue, Staff, Role, OrgType, Organization, Follow
from App_Organization.forms import ApplicationAdminForm, OrganizationAdminForm
# Register your models here.


class ApplicationAdmin(admin.ModelAdmin):
    form = ApplicationAdminForm
    list_display = ('organization_name', 'user', 'orgtype')


class OrganizationAdmin(admin.ModelAdmin):
    form = OrganizationAdminForm
    list_display = ('organization_name', 'orgtype')


admin.site.register(Application, ApplicationAdmin)
admin.site.register(League)
admin.site.register(Venue)
admin.site.register(Staff)
admin.site.register(Role)
admin.site.register(Organization, OrganizationAdmin)
admin.site.register(OrgType)
admin.site.register(Follow)
