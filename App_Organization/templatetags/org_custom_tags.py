from django import template
from django.template.defaultfilters import stringfilter


register = template.Library()


@register.filter(name='initials', is_safe=True)
@stringfilter
def inititals(value):
    words = value.split()
    initials = ''
    if len(words) > 2:
        x = 1
    else:
        x = len(words) - 1
    while x >= 0:
        initials = words[x][0].upper() + initials
        x -= 1
    return initials
