from django.urls import path
from App_Organization import views
from App_Invitation.views import start_invitation, user_invites
from App_Shop import views as ShopViews
from App_Event import views as EventViews


app_name = "App_Organization"

urlpatterns = [
    path("", views.home, name="home"),
    path(
        "success/",
        ShopViews.CheckoutSuccess.as_view(),
        name="checkout_success",
    ),
    path("cancel/", ShopViews.CheckoutCancel.as_view(), name="checkout_cancel"),
    # stripe auth
    path(
        "authorize/<uuid>/",
        views.StripeAuthorizeView.as_view(),
        name="authorize",
    ),
    path(
        "oauth/callback/",
        views.StripeAuthorizeCallbackView.as_view(),
        name="authorize_callback",
    ),
    path("webhooks/stripe/", ShopViews.stripe_webhook, name="stripe-webhook"),
    #
    path("my-events/", EventViews.UserEvents.as_view(), name="my_events"),
    path(
        "manage/<str:org_type>",
        views.ManageOrganizations.as_view(),
        name="my_organizations",
    ),
    # path to normalize viewing all invitations from organizations
    # ex. current_site/my-invites
    path("my-invites/", user_invites, name="my_invites"),
    path("approve/<pk>/", views.approve_application, name="approve"),
    path("reject/<pk>/", views.reject_application, name="reject"),
    path("archive/<pk>/", views.archive_application, name="archive"),
    path(
        "application/<str:org_type>",
        views.application_custom,
        name="application",
    ),
    path(
        "application/status/",
        views.application_status,
        name="application_status",
    ),
    path(
        "application/<pk>/",
        views.ApplicationDetailView.as_view(),
        name="application_detail",
    ),
    path("<slug:slug>/", views.organization_view, name="organization"),
    path("<slug:slug>/follow/", views.follow, name="follow"),
    path("<slug:slug>/unfollow/", views.unfollow, name="unfollow"),
    path("<slug:slug>/staff/", views.organization_staff, name="staff"),
    path("<slug>/update/", views.edit_organization, name="edit_organization"),
    # path to normalize starting invitations from organizations
    # ex. current_site/league-slug/invite
    path("<slug>/invite/", start_invitation, name="invite"),
    # Products
    path(
        "<slug>/new-product/",
        ShopViews.NewProductView.as_view(),
        name="new_product",
    ),
    path(
        "<slug>/products/",
        ShopViews.ProductListView.as_view(),
        name="products",
    ),
    path(
        "<slug>/products/<pk>/",
        ShopViews.ProductDetail.as_view(),
        name="product_detail",
    ),
    # Events
    path("<slug>/new-event/", EventViews.NewEvent.as_view(), name="new_event"),
    path(
        "<slug>/new-event/<pk>/event-detail/",
        EventViews.NewEventDetail.as_view(),
        name="new_event_detail",
    ),
    path(
        "<slug>/new-event/<pk>/event-cart/",
        EventViews.BuildEventCart.as_view(),
        name="new_event_cart",
    ),
    path(
        "<slug>/new-event/<pk>/event-cart/add/",
        EventViews.AddToEventCart.as_view(),
        name="event_cart_add",
    ),
    path(
        "<slug>/new-event/<pk>/event-cart/remove/",
        EventViews.RemoveFromEventCart.as_view(),
        name="event_cart_remove",
    ),
    path(
        "<slug>/new-event/<pk>/review/",
        EventViews.EventReview.as_view(),
        name="event_review",
    ),
    path(
        "<slug>/new-event/<pk>/submit/",
        EventViews.EventFinalSubmit.as_view(),
        name="event_submit",
    ),
    path(
        "<slug>/event/<pk>/review/",
        EventViews.EventOrgReview.as_view(),
        name="event_org_review",
    ),
    path(
        "<slug>/event/<pk>/approve/",
        EventViews.EventApprove.as_view(),
        name="event_approve",
    ),
    path(
        "<pk>/payment/",
        ShopViews.GetPaymentIntent.as_view(),
        name="payment_intent",
    ),
    path(
        "<slug:slug>/event/<int:event>/cart/<int:cart>/checkout/",
        ShopViews.CartPayment.as_view(),
        name="cart_payment",
    ),
    path(
        "<pk>/invoice/",
        ShopViews.CreateInvoice.as_view(),
        name="create_invoice",
    ),
    path(
        "<slug>/event/<pk>/staff-toggle/",
        EventViews.EventStaffRegToggle.as_view(),
        name="staff_toggle",
    ),
    path(
        "<slug>/event-staff/<pk>/add/",
        EventViews.StaffEventRegAdd.as_view(),
        name="staff_add",
    ),
]
