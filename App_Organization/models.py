import uuid
from django.db import models
from django.template.defaultfilters import slugify
from django.conf import settings
from django.urls import reverse
from App_Login.models import User

from phone_field import PhoneField
from localflavor.us.models import USZipCodeField, USStateField


# Create your models here.


class OrgType(models.Model):
    orgtype = models.CharField(max_length=10)

    def __str__(self):
        return self.orgtype


class Organization(models.Model):
    orgtype = models.ForeignKey(OrgType, on_delete=models.CASCADE, related_name='org_type')
    slug = models.SlugField(max_length=255, unique='true')
    organization_name = models.CharField(max_length=80, unique=True, null=False)
    organization_image = models.ImageField(upload_to='organization_images/', null=True, blank=True)
    description = models.TextField(help_text='Description of offered services')
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    address_1 = models.CharField(max_length=255)
    address_2 = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=30)
    state = USStateField()
    postal_code = USZipCodeField()
    phone = PhoneField(help_text='Main Contact Phone Number')
    created = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['orgtype', 'organization_name']

    def __str__(self):
        return f'{self.organization_name}'

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.organization_name)
        return super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('organization', kwargs={'slug': self.slug})


class Application(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='application')
    orgtype = models.ForeignKey(OrgType, on_delete=models.CASCADE, related_name='app_orgtype')
    organization_name = models.CharField(max_length=80)
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    address_1 = models.CharField(max_length=255)
    address_2 = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=30)
    state = USStateField()
    postal_code = USZipCodeField()
    phone = PhoneField(help_text='Main Contact Phone Number')
    service_description = models.TextField(help_text='Description of offered services')
    application_date = models.DateTimeField(auto_now_add=True)
    approved = models.BooleanField(default=False)
    approved_date = models.DateTimeField(null=True, blank=True)
    archived = models.BooleanField(default=False)
    rejected = models.BooleanField(default=False)
    rejected_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return f'{ self.user.profile.get_profile_firstlast()} | {self.organization_name} | Type: {self.orgtype.orgtype}'


class League(models.Model):
    organization = models.OneToOneField(Organization, on_delete=models.CASCADE)
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    stripe_user_id = models.CharField(max_length=255, blank=True)
    stripe_access_token = models.CharField(max_length=255, blank=True)
    stripe_refresh_token = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return f'{self.organization.organization_name}'


class Venue(models.Model):
    organization = models.OneToOneField(Organization, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.organization.organization_name}'


class Role(models.Model):
    role_type = models.ForeignKey(OrgType, on_delete=models.CASCADE, related_name='role_type')
    role = models.CharField(max_length=30)

    class Meta:
        unique_together = ['role', 'role_type']
        ordering = ['role_type']

    def __str__(self):
        return f'{self.role_type} {self.role}'


class Staff(models.Model):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, related_name='staff_organization')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='staff_user')
    roles = models.ManyToManyField(Role)

    class Meta:
        ordering = ['organization']
        verbose_name_plural = 'Staff'
        unique_together = ['organization', 'user']

    def __str__(self):
        return f'{self.organization.organization_name} | {self.organization.orgtype} |{self.user.profile.get_profile_firstlast()}'


class Follow(models.Model):
    follower = models.ForeignKey(User, on_delete=models.CASCADE, related_name='follow_user')
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, related_name='follow_organization')

    class Meta:
        ordering = ['organization']
        unique_together = ['follower', 'organization']

    def __str__(self):
        return f'{self.follower.profile.get_profile_firstlast()} follows {self.organization.organization_name}'
