# Generated by Django 3.1.5 on 2021-03-02 17:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('App_Organization', '0005_auto_20210222_0949'),
    ]

    operations = [
        migrations.AlterField(
            model_name='league',
            name='organization',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='App_Organization.organization'),
        ),
    ]
