import urllib
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import HttpResponse
from django.views.generic.base import TemplateView
import requests

from django.shortcuts import (
    render,
    HttpResponseRedirect,
    reverse,
    get_object_or_404,
    redirect,
)
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django_staff_required.views import StaffRequiredMixin
from django.views.generic import DetailView
from django.core.mail import send_mail, EmailMessage
from django.utils import timezone
from django.conf import settings
from django.utils.html import format_html
from django.views import View

from django.contrib.sites.shortcuts import get_current_site

from App_Login.views import PageTitleMixin

from App_Organization.forms import ApplicationForm, OrganizationEdit
from App_Post.forms import NewPostForm, NewPostImageForm

from App_Organization.models import (
    Application,
    League,
    Venue,
    Staff,
    Role,
    Organization,
    OrgType,
    Follow,
)
from App_Invitation.models import OrganizationInvitation
from App_Post.models import Post, PostImage, Like

from App_Organization.decorators import check_org_admin


class ApplicationDetailView(StaffRequiredMixin, PageTitleMixin, DetailView):
    model = Application
    context_object_name = "application"
    page_title = "Application Detail"
    template_name = "App_Organization/application_detail.html"


#  use the url parameter <str:org_type> to determine form org_type and customize template


@login_required
def application_custom(request, org_type):
    context = {}
    success_url_reverse = "App_Organization:application_status"
    context["page_title"] = org_type + " Application"
    # Application form requires org as argument. org may be None
    form = ApplicationForm()
    context["form"] = form
    if request.method == "POST":
        form = ApplicationForm(request.POST)
        context["form"] = form
        if form.is_valid():
            # check for an existing application using organization name
            application_check = Application.objects.filter(
                organization_name=form.cleaned_data["organization_name"]
            )
            org_name_check = form.cleaned_data["organization_name"]
            if len(application_check) > 0:
                messages.error(
                    request,
                    f"An application for {org_name_check} already exists.",
                )
                return render(
                    request,
                    "App_Organization/application.html",
                    context=context,
                )
            new_org_type = OrgType.objects.get(orgtype=org_type)
            application, created = Application.objects.get_or_create(
                user=request.user,
                orgtype=new_org_type,
                organization_name=form.cleaned_data["organization_name"],
                first_name=form.cleaned_data["first_name"],
                last_name=form.cleaned_data["last_name"],
                address_1=form.cleaned_data["address_1"],
                address_2=form.cleaned_data["address_2"],
                city=form.cleaned_data["city"],
                state=form.cleaned_data["state"],
                postal_code=form.cleaned_data["postal_code"],
                phone=form.cleaned_data["phone"],
                service_description=form.cleaned_data["service_description"],
            )
            messages.success(request, "Your application has been submitted.")
            message_body = f"{request.user.profile.first_name}, Your league application for {application.organization_name} has been submitted and is under review."
            send_mail(
                "Your Application Has Been Submitted",
                message_body,
                settings.NO_REPLY_EMAIL,
                [request.user.email],
                fail_silently=False,
            )
            return HttpResponseRedirect(reverse(success_url_reverse))
    return render(request, "App_Organization/application.html", context=context)


def home(request):
    context = {}
    context["page_title"] = "LeagueBox"
    if request.user.is_authenticated:
        context["page_title"] = "LeagueBox Home"
        following_list = Follow.objects.filter(follower=request.user)
        posts = Post.objects.filter(
            organization__in=following_list.values_list("organization")
        )
        context["posts"] = posts
        if not request.user.profile.is_fully_filled():
            messages.warning(
                request, "Please complete required fields before continuing."
            )
            return HttpResponseRedirect(reverse("App_Login:profile"))
        invitations = OrganizationInvitation.objects.filter(
            invitation__email=request.user.email,
            invitation__expire_date__gte=timezone.now(),
        )
        if len(invitations) > 0:
            message = format_html(
                'You have pending invitations! Click <a href="{}">here</a>.',
                reverse("App_Organization:my_invites"),
            )
            messages.info(request, message)
        return render(request, "App_Organization/home.html", context=context)
    return render(request, "landing.html", context={})


@login_required
def application_status(request):
    context = {}
    context["page_title"] = "Application Status"
    applications = None
    if request.user.is_staff:
        application_qs = Application.objects.filter(archived=False).order_by("id")
        if application_qs.exists():
            applications = application_qs
            exception_count = 0
            for application in applications:
                if not application.approved and not application.rejected:
                    exception_count += 1
            if exception_count > 0:
                messages.warning(request, "You have pending applications to review")
    else:
        application_qs = Application.objects.filter(user=request.user, archived=False)
        if application_qs.exists():
            applications = application_qs
        else:
            messages.warning(request, "You have no pending applications")
            return render(
                request,
                "App_Organization/application_status.html",
                context=context,
            )
    context["applications"] = applications
    return render(request, "App_Organization/application_status.html", context=context)


@staff_member_required
def approve_application(request, pk):
    application = get_object_or_404(Application, pk=pk)
    # except Application.DoesNotExist:
    #     messages.error(request, 'Application does not exist. Something went wrong')
    #     return HttpResponseRedirect(reverse('App_Organization:application_status'))
    application_org_type = application.orgtype
    organization, created = Organization.objects.get_or_create(
        orgtype=application_org_type,
        organization_name=application.organization_name,
        first_name=application.first_name,
        last_name=application.last_name,
        address_1=application.address_1,
        address_2=application.address_2,
        city=application.city,
        state=application.state,
        phone=application.phone,
        description=application.service_description,
    )
    organization_test_qs = Organization.objects.filter(
        organization_name=application.organization_name
    )

    if not organization_test_qs.exists():
        messages.error(request, "Organization not created. Something went wrong")
        return HttpResponseRedirect(reverse("App_Organization:application_status"))

    # test for organization type, then add organization to the appropriate model
    if application_org_type.orgtype == League.__name__:
        league, created = League.objects.get_or_create(organization=organization)
        league_test_qs = League.objects.filter(organization=organization)
        if not league_test_qs.exists():
            messages.error(request, "League not created. Something went wrong")
            return HttpResponseRedirect(reverse("App_Organization:application_status"))

    if application_org_type.orgtype == Venue.__name__:
        venue, created = Venue.objects.get_or_create(organization=organization)
        venue_test_qs = Venue.objects.filter(organization=organization)
        if not venue_test_qs.exists():
            messages.error(request, "Venue not created. Something went wrong")
            return HttpResponseRedirect(reverse("App_Organization:application_status"))

    # Mark application as approved
    application.approved = True
    application.approved_date = timezone.now()
    application.save()

    # Add application submission user to organization staff as admin
    staffadmin = Role.objects.get(role="Admin", role_type=application_org_type)
    staff = Staff(
        user=application.user,
        organization=organization,
    )
    staff.save()
    staff.roles.add(staffadmin)

    # Send Email Notification to Application User
    current_site = get_current_site(request)
    email = EmailMessage(
        subject=f"{application_org_type.orgtype} Approved",
        body=f"Your {application_org_type.orgtype} application for {organization.organization_name} has been approved. Visit {current_site.domain}:8000/{organization.slug}",
        from_email=settings.NO_REPLY_EMAIL,
        to=[application.user.email],
        reply_to=[settings.NO_REPLY_EMAIL],
    )
    email.send()

    return HttpResponseRedirect(reverse("App_Organization:application_status"))


def reject_application(request, pk):
    try:
        application = Application.objects.get(pk=pk)
    except Application.DoesNotExist:
        messages.error(request, "Application does not exist. Something went wrong")
        return HttpResponseRedirect(reverse("App_Organization:application_status"))

    # Mark application as rejected
    application.rejected = True
    application.rejected_date = timezone.now()
    application.save()

    # Send Email Notification to Application User
    email = EmailMessage(
        subject="League Application Rejected",
        body=f"Your league application for {application.organization_name} has been rejected. Please resubmit your application",
        from_email=settings.NO_REPLY_EMAIL,
        to=[application.user.email],
        reply_to=[settings.NO_REPLY_EMAIL],
    )
    email.send()
    messages.info(
        request,
        f"Application #{application.id} for {application.organization_name} has been rejected.",
    )
    return HttpResponseRedirect(reverse("App_Organization:application_status"))


@staff_member_required
def archive_application(request, pk):
    try:
        application = Application.objects.get(pk=pk)
    except Application.DoesNotExist:
        messages.error(request, "Application does not exist. Something went wrong")
        return HttpResponseRedirect(reverse("App_Organization:application_status"))

    # Mark application as rejected
    application.archived = True
    application.save()
    messages.info(
        request,
        f"Application #{application.id} for {application.organization_name} has been archived.",
    )
    return HttpResponseRedirect(reverse("App_Organization:application_status"))


def check_roles(organization, user):
    staff = Staff.objects.filter(organization=organization, user=user)
    org_type = organization.orgtype.orgtype
    user_roles = []
    admin_bool = False
    if len(staff) > 1:
        messages.error("Error checking for Staff Role. Please notify Administrator")
        admin_bool = False
        return user_roles, admin_bool
    if len(staff) > 0:
        for person in staff.iterator():
            roles = person.roles.all()
            for role in roles:
                role_pair = (role.role_type.orgtype, role.role)
                user_roles.append(role_pair)
        for role in user_roles:
            if role == (org_type, "Admin"):
                admin_bool = True
    else:
        admin_bool = False
        user_roles = []
    return user_roles, admin_bool


def organization_view(request, slug):
    context = {}
    organization = get_object_or_404(Organization, slug=slug)
    context["page_title"] = organization.organization_name
    context["organization"] = organization
    # qs posts for this organization
    posts = Post.objects.filter(organization__slug=slug)
    context["posts"] = posts
    # get number of followers
    followers_count = len(Follow.objects.filter(organization=organization))
    context["followers"] = followers_count
    user_followed = []
    post_form = None
    post_image_form = None

    if request.user.is_authenticated:
        user_followed = Follow.objects.filter(
            organization=organization, follower=request.user
        )
        context["user_followed"] = user_followed
        liked_post = Like.objects.filter(user=request.user)
        liked_post_list = liked_post.values_list("post", flat=True)
        context["liked_post_list"] = liked_post_list
        post_form = NewPostForm()
        post_image_form = NewPostImageForm()
        context["post_form"] = post_form
        context["post_image_form"] = post_image_form
        if request.method == "POST":
            post_form = NewPostForm(request.POST)
            post_image_form = NewPostImageForm(request.POST, request.FILES)
            context["post_form"] = post_form
            context["post_image_form"] = post_image_form
            if post_form.is_valid() and post_image_form.is_valid():
                new_post = Post(organization=organization)
                new_post.title = post_form.cleaned_data["title"]
                new_post.content = post_form.cleaned_data["content"]
                new_post.commenting = post_form.cleaned_data["commenting"]
                new_post.save()
                if request.FILES:
                    new_post_image = PostImage.objects.create(
                        image=request.FILES["image"], post=new_post
                    )
                    new_post_image.caption = post_image_form.cleaned_data["caption"]
                    new_post_image.save()
                return HttpResponseRedirect(
                    reverse(
                        "App_Organization:organization",
                        kwargs={"slug": organization.slug},
                    )
                )
    user_roles = []
    admin = False
    if request.user.is_authenticated:
        user_roles, admin = check_org_admin(
            get_roles=True, slug=organization.slug, user=request.user
        )
        context["user_roles"] = user_roles
        context["admin"] = admin
    return render(request, "App_Organization/organization.html", context=context)


@login_required
def edit_organization(request, slug):
    context = {}
    user_roles = []
    admin = False
    organization = get_object_or_404(Organization, slug=slug)
    context["page_title"] = "Edit " + organization.organization_name
    form = OrganizationEdit(instance=organization)
    context["form"] = form
    if request.user.is_authenticated:
        user_roles, admin = check_roles(organization, request.user)
    if not user_roles and not admin:
        messages.error(
            request,
            "You do not have the privileges to access this admin page.",
        )
        return HttpResponseRedirect(
            reverse(
                "App_Organization:organization",
                kwargs={"slug": organization.slug},
            )
        )
    if request.method == "POST":
        form = OrganizationEdit(request.POST, request.FILES, instance=organization)
        context["form"] = form
        if form.is_valid():
            for fields in form.cleaned_data:
                image = request.FILES.get("organization_image")
            if image:
                organization.organization_image = image
                organization.save()
            if request.POST.get("organization_image-clear"):
                organization.organization_image = None
                organization.save()
        return HttpResponseRedirect(
            reverse(
                "App_Organization:edit_organization",
                kwargs={"slug": organization.slug},
            )
        )
    return render(request, "App_Organization/edit_organization.html", context=context)


@login_required
def organization_staff(request, slug):
    context = {}
    organization = get_object_or_404(Organization, slug=slug)
    context["organization"] = organization
    staff = Staff.objects.filter(organization=organization)
    context["staff"] = staff
    context["page_title"] = str(organization.organization_name) + " Staff List"
    return render(
        request,
        "App_Organization/organization_staff_list.html",
        context=context,
    )


class ManageOrganizations(LoginRequiredMixin, TemplateView):
    template_name = "App_Organization/manage_organizations.html"

    def get_context_data(self, **kwargs):
        context = super(ManageOrganizations, self).get_context_data(**kwargs)
        self.user = self.request.user
        self.org_type = kwargs["org_type"]
        self.user_org = None
        if self.user:
            self.user_org = Staff.objects.filter(user=self.user)
        self.organization_list = []
        for person in self.user_org:
            for role in person.roles.all():
                if role.role == "Admin" and role.role_type.orgtype == self.org_type:
                    self.organization_list.append(person.organization)
        print(self.organization_list)
        if not self.organization_list:
            message = format_html(
                'You have no registered {}s. Apply <a href="{}">here</a>.',
                self.org_type,
                reverse(
                    "App_Organization:application",
                    kwargs={"org_type": self.org_type},
                ),
            )
            messages.info(self.request, message)
            return redirect(reverse("App_Organization:home"))
        context["organization_list"] = self.organization_list
        context["page_title"] = "Manage " + self.org_type + "s"
        return context


@login_required
def follow(request, slug):
    organization = Organization.objects.get(slug=slug)
    already_followed = Follow.objects.filter(
        follower=request.user, organization=organization
    )
    if not already_followed:
        user_follow = Follow(follower=request.user, organization=organization)
        user_follow.save()
    return HttpResponseRedirect(
        reverse("App_Organization:organization", kwargs={"slug": organization.slug})
    )


@login_required
def unfollow(request, slug):
    organization = Organization.objects.get(slug=slug)
    already_followed = Follow.objects.filter(
        follower=request.user, organization=organization
    )
    if already_followed:
        user_follow = Follow.objects.get(
            follower=request.user, organization=organization
        )
        user_follow.delete()
    return HttpResponseRedirect(
        reverse("App_Organization:organization", kwargs={"slug": organization.slug})
    )


class StripeAuthorizeView(View):
    def get(self, request, uuid):
        self.uuid = uuid
        if not self.request.user.is_authenticated:
            return HttpResponseRedirect(reverse("App_Login:login"))
        url = "https://connect.stripe.com/oauth/authorize"
        params = {
            "response_type": "code",
            "scope": "read_write",
            "client_id": settings.STRIPE_CONNECT_CLIENT_ID,
            "redirect_uri": "http://localhost:8000/oauth/callback",
            "state": self.uuid,
        }
        url = f"{url}?{urllib.parse.urlencode(params)}"
        return redirect(url)


class StripeAuthorizeCallbackView(View):
    def get(self, request):
        state = request.GET.get("state")
        organization = Organization.objects.get(league__uuid=state)
        code = request.GET.get("code")
        if code:
            data = {
                "client_secret": settings.STRIPE_SECRET_KEY,
                "grant_type": "authorization_code",
                "client_id": settings.STRIPE_CONNECT_CLIENT_ID,
                "code": code,
            }
            url = "https://connect.stripe.com/oauth/token"
            resp = requests.post(url, params=data)

            print(resp.json())
            # add stripe info to the seller
            stripe_user_id = resp.json()["stripe_user_id"]
            stripe_access_token = resp.json()["access_token"]
            stripe_refresh_token = resp.json()["refresh_token"]
            # seller = Seller.objects.filter(user_id=self.request.user.id).first()
            organization.league.stripe_access_token = stripe_access_token
            organization.league.stripe_refresh_token = stripe_refresh_token
            organization.league.stripe_user_id = stripe_user_id
            organization.league.save()
            messages.add_message(
                self.request, messages.SUCCESS, "Stripe Connected Successfully"
            )

        url = reverse("App_Organization:my_organizations", kwargs={"org_type": "League"})
        response = redirect(url)
        return response
